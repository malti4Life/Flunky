package com.flunky.Activities;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.flunky.FlunkyApplication;
import com.flunky.Model.UserModel;
import com.flunky.Model.ResponseOfAllApiModel;
import com.flunky.R;
import com.flunky.facebook.FacebookAPI;
import com.flunky.facebook.FacebookInterface;
import com.flunky.facebook.Method;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;


import retrofit2.Call;

import static android.content.ContentValues.TAG;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private LinearLayout login_ll_login_view;
    private LinearLayout login_ll_forgot_password_view;
    private TextView login_tv_forgot_pwd;
    private TextView login_tv_submit;
    private TextView login_tv_signup;
    private TextView login_tv_facebook;
    private TextView login_tv_forgot_pwd_retrieve;
    private EditText login_et_email;
    private EditText logint_et_forgot_pwd_email;
    private EditText login_et_password;
    private JsonObject userObject;
    private UserModel userModel;
    private int userAccountType = 0;



    @Override
    protected void initView() {

        checkLocation();
        new FacebookAPI();
        FacebookAPI.getInstance().init(this);
        FacebookAPI.getInstance().setPermissions(Constant.permissions);
        login_ll_login_view=(LinearLayout)findViewById(R.id.login_ll_view);
        login_ll_forgot_password_view=(LinearLayout)findViewById(R.id.login_ll_forgot_pwd_view);

        login_et_email = (EditText) findViewById(R.id.login_et_email);
        logint_et_forgot_pwd_email=(EditText)findViewById(R.id.login_forgot_pwd_et_email);
        login_et_password = (EditText) findViewById(R.id.login_et_pwd);

        login_tv_forgot_pwd = (TextView) findViewById(R.id.login_tv_forgot_pwd);
        login_tv_facebook = (TextView) findViewById(R.id.login_tv_fb_login);
        login_tv_submit = (TextView) findViewById(R.id.login_tv_submit);
        login_tv_signup = (TextView) findViewById(R.id.login_tv_signup);
        login_tv_forgot_pwd_retrieve=(TextView)findViewById(R.id.login_tv_forgot_pwd_retrieve);

        login_tv_forgot_pwd.setOnClickListener(this);
        login_tv_signup.setOnClickListener(this);
        login_tv_submit.setOnClickListener(this);
        login_tv_facebook.setOnClickListener(this);
        login_tv_forgot_pwd_retrieve.setOnClickListener(this);
    }



    @Override
    protected void initToolBar() {

    }

    public void onFacebookClick(View view) {
        FacebookAPI.getInstance().logout();
        FacebookAPI.getInstance().facebookMeRequest(this, Method.PROFILE, new FacebookInterface() {
            @Override
            public void getMyProfile(JSONObject jsonObject) {
                userObject = new JsonObject();
                final String id = jsonObject.optString("id");
                userObject.addProperty("email", jsonObject.optString("email"));
                userObject.addProperty("firstname", jsonObject.optString("first_name"));
                userObject.addProperty("lastname", jsonObject.optString("last_name"));
                userObject.addProperty("fb_id", id);
                userObject.addProperty("device_type", "android");
                userObject.addProperty("login_from", 1);
                final String gcm_key = Utils.getString(LoginActivity.this, Constant.GCM_UNIQUE_ID);
                userObject.addProperty("token_key", gcm_key);
                final String userEmail = userObject.get("email").getAsString();

                if (userEmail.equalsIgnoreCase("")) {
                    Logger.showSnackBar(LoginActivity.this, "Sorry we are not able to get your email id from facebook.Kindly signUp");
                    final Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                    navigateToNextActivity(intent, false);
                } else {

                    final String firstName = userObject.get("firstname").getAsString();
                    final String lastName = userObject.get("lastname").getAsString();
                    final String fbId = userObject.get("fb_id").getAsString();
                    final String device_token = Utils.getString(LoginActivity.this,Constant.GCM_UNIQUE_ID);
                    userAccountType = userObject.get("login_from").getAsInt();
                    final String deviceId = Utils.getUniqueDeviceId(LoginActivity.this);
                    final String apiKey = "flunkyapp";
                    userModel = new UserModel();
                    userModel.setFirstName(firstName);
                    userModel.setLastName(lastName);
                    userModel.setApiKey(apiKey);
                    userModel.setEmail(userEmail);
                    userModel.setMobile("");
                    userModel.setPasword("");
                    userModel.setPass_confirm("");
                    userModel.setUserAccountType(userAccountType);
                    userModel.setFacebookId(fbId);

                    doLogin(userEmail, fbId, "", userAccountType, apiKey, deviceId, device_token);

                }
            }
        });
    }
//
//    private void getFriendlist(String facebookId) {
//
//        new GraphRequest(
//                AccessToken.getCurrentAccessToken(),
//                "/" + facebookId + "/friends",
//                null,
//                HttpMethod.GET,
//                new GraphRequest.Callback() {
//                    public void onCompleted(GraphResponse response) {
//                        Log.e("List", response.toString());
//            /* handle the result */
//                    }
//                }
//        ).executeAsync();
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (FacebookAPI.getInstance().getCallbackManager() != null) {
            FacebookAPI.getInstance().getCallbackManager().onActivityResult(requestCode, resultCode, data);
        }
        switch (requestCode) {
            case Constant.REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:

                        break;
                    case Activity.RESULT_CANCELED:
                        Logger.toast(this, getString(R.string.enable_gps));
                        break;
                }
                break;
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }


    @Override
    public void onClick(View v) {
        Intent i;
        int id = v.getId();
        switch (id) {
            case R.id.login_tv_forgot_pwd:
                login_ll_forgot_password_view.setVisibility(View.VISIBLE);
                login_ll_login_view.setVisibility(View.GONE);
                break;
            case R.id.login_tv_signup:
                i = new Intent(LoginActivity.this, SignUpActivity.class);
                navigateToNextActivity(i, false);
                break;
            case R.id.login_tv_submit:
                Utils.hideSoftKeyboard(this);
                validateData();
                break;
            case R.id.login_tv_fb_login:
                onFacebookClick(v);
                break;
            case R.id.login_tv_forgot_pwd_retrieve:
                Utils.hideSoftKeyboard(this);
                validateForgotPwd();
                break;
        }
    }

    private void validateForgotPwd() {
        final String email = logint_et_forgot_pwd_email.getText().toString();
        if (!TextUtils.isEmpty(email) && Utils.isEmailValid(email)) {
             forgotPwdCall(email);
        } else {
            Logger.showSnackBar(this, getString(R.string.enter_valid_email));
        }
    }

    private void forgotPwdCall(String email) {
        Call<JsonObject>forgotPwdCall = new RestClient().getApiInterface().forgotPwdCall(Constant.APP_KEY,email,Constant.OS);
        forgotPwdCall.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get("res_code").getAsString().equalsIgnoreCase("0")){

                }else{
                    login_ll_forgot_password_view.setVisibility(View.GONE);
                    login_ll_login_view.setVisibility(View.VISIBLE);
                }
               Logger.toast(LoginActivity.this,data.get("res_message").toString());

            }
        });
    }

    @Override
    public void onBackPressed() {
        if(login_ll_forgot_password_view.getVisibility()==View.VISIBLE){
            login_ll_forgot_password_view.setVisibility(View.GONE);
            login_ll_login_view.setVisibility(View.VISIBLE);
        }else{
            finish();
        }
    }

    private void validateData() {
        final String email = login_et_email.getText().toString();
        final String password = login_et_password.getText().toString();
        if (!TextUtils.isEmpty(email) && Utils.isEmailValid(email)) {
            if (!TextUtils.isEmpty(password)) {
                doLogin(email, "", password, userAccountType, Constant.APP_KEY, Utils.getUniqueDeviceId(LoginActivity.this), Utils.getString(LoginActivity.this,Constant.GCM_UNIQUE_ID));
            } else {
                Logger.showSnackBar(this, getString(R.string.enter_password));
            }
        } else {
            Logger.showSnackBar(this, getString(R.string.enter_valid_email));
        }
    }

    private void doLogin(final String email, String fbid, String password, final int userAccount, String apiKey, String deviceId, String device_token) {
        final ProgressDialog dialog = Logger.showProgressDialog(this);
        Call<ResponseOfAllApiModel> loginCall = new RestClient().getApiInterface().doLogin(email, fbid, password, userAccount, apiKey, deviceId, device_token,Constant.OS);
        loginCall.enqueue(new RetrofitCallback<ResponseOfAllApiModel>(this, dialog) {
            @Override
            public void onSuccess(ResponseOfAllApiModel data) {
                Utils.storeString(LoginActivity.this, Constant.KEY_USER_DATA, new Gson().toJson(data.getUserResponseModel()));
                Logger.toast(LoginActivity.this, getString(R.string.welcome_msg));
                Intent i = new Intent(LoginActivity.this, MainActivity.class);
                navigateToNextActivity(i, true);

            }
            @Override
            public void onFailure(Call<ResponseOfAllApiModel> call, Throwable error) {
                dialog.dismiss();
                    if (userAccount == 1) {
                        Utils.storeString(LoginActivity.this, Constant.KEY_DATA, new Gson().toJson(userModel));
                        final Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                        navigateToNextActivity(intent, false);
                    } else {
                        Logger.toast(LoginActivity.this, error.getMessage());
                    }

            }
        });
    }
    private void checkLocation() {
        final int finePermissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION);
        final int coarsePermissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        final int readPermissionCheck = ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_PHONE_STATE);
        if (finePermissionCheck != PackageManager.PERMISSION_GRANTED && coarsePermissionCheck != PackageManager.PERMISSION_GRANTED
                && readPermissionCheck != PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION
                                , android.Manifest.permission.READ_PHONE_STATE},
                        Constant.REQUEST_CODE_ASK_PERMISSIONS);
            }
        } else {
            final double latitude = FlunkyApplication.getInstance().getLatitude();
            final double longitude = FlunkyApplication.getInstance().getLongitude();
            if (latitude == 0 && longitude == 0) {
                showLocationEnableDialog();
            }
        }

    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constant.REQUEST_CODE_ASK_PERMISSIONS:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    final double latitude = FlunkyApplication.getInstance().getLatitude();
                    final double longitude = FlunkyApplication.getInstance().getLongitude();
                    if (latitude == 0 && longitude == 0) {
                        showLocationEnableDialog();
                    }
                    Logger.w("Enabled locations");
                } else {
                    Logger.toast(this, "Please enable location from Apps setting");

                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public void showLocationEnableDialog() {
        final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(FlunkyApplication.getInstance().getLocationRequest());
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(FlunkyApplication.getInstance().getGoogleApiClient(), builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        FlunkyApplication.getInstance().onConnected(null);
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(LoginActivity.this, Constant.REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

}
