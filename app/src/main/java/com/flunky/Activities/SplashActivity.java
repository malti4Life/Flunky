package com.flunky.Activities;

import android.content.Intent;
import android.os.CountDownTimer;
import android.text.TextUtils;

import com.flunky.fcm.MyFirebaseInstanceIDService;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;

public class SplashActivity extends BaseActivity {

    @Override
    protected void initView() {
     new SplashCountDown(2000,1000).start();
    }

    @Override
    protected void initToolBar() {

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_splash;
    }

   private class SplashCountDown extends CountDownTimer {
       /**
        * @param millisInFuture    The number of millis in the future from the call
        *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
        *                          is called.
        * @param countDownInterval The interval along the way to receive
        *                          {@link #onTick(long)} callbacks.
        */
       public SplashCountDown(long millisInFuture, long countDownInterval) {
           super(millisInFuture, countDownInterval);
       }

       @Override
       public void onTick(long millisUntilFinished) {

       }

       @Override
       public void onFinish() {
          if(Utils.isConnectedToInternet(SplashActivity.this)){
              final String user_id = Utils.getString(SplashActivity.this, Constant.User_id);

              Intent intent;
              if (!TextUtils.isEmpty(user_id)) {
                  intent = new Intent(SplashActivity.this, MainActivity.class);
              } else {
                  intent = new Intent(SplashActivity.this, LoginActivity.class);
              }
              navigateToNextActivity(intent, true);
          }else{
              Logger.toast(SplashActivity.this,"No internet Connection");
              finish();
          }

       }

   }

    @Override
    protected void onStart() {
        super.onStart();
        if(Utils.checkPlayServices(SplashActivity.this)){
            Intent intent = new Intent(this, MyFirebaseInstanceIDService.class);
            startService(intent);
        }

    }
}
