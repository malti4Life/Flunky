package com.flunky.Activities;

import android.content.DialogInterface;
import android.content.Intent;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;

import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flunky.Fragments.AboutFlunkyFragment;
import com.flunky.Fragments.CalendarFragment;

import com.flunky.Fragments.PastVoucherFragment;
import com.flunky.Fragments.ProfileFragment;
import com.flunky.Fragments.SearchFragment;
import com.flunky.Fragments.WalletFragment;

import com.flunky.Model.UserResponseModel;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.TabType;
import com.flunky.utils.Utils;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import retrofit2.Call;

public class MainActivity extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private FrameLayout row_tool_bar_ll_menu;

    private LinearLayout wallet;
    private LinearLayout search;
    private LinearLayout calendar;
    private TextView row_tool_bar_tv_wallet_name;
    private TextView row_tool_bar_tv_second_tab_title;
    private TextView row_tool_bar_tv_second_tab_subTitle;
    private ImageView menuImage;
    private RadioGroup radioGroup;
    private static MainActivity instance;
    private TabType tabType;
    private UserResponseModel userResponseModel;

    @Override
    protected void initView() {
        Intent intent = getIntent();
        instance = this;
        radioGroup = (RadioGroup) findViewById(R.id.layout_left_drawer_rv);
        radioGroup.setOnCheckedChangeListener(this);
        Logger.e(Utils.getString(this,Constant.GCM_UNIQUE_ID));
        final String fromNoti=intent.getStringExtra("FromNotification");
        if (!TextUtils.isEmpty(fromNoti)) {
            tabType = TabType.WALLET;
            setBackground(tabType);
            displayFragment(new WalletFragment());
        }else{
            displayFragment(new SearchFragment());
        }
    }


    @Override
    protected void initToolBar() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        toolbar = (Toolbar) findViewById(R.id.activity_main_toolbar);
        row_tool_bar_ll_menu = (FrameLayout) toolbar.findViewById(R.id.row_toolbar_ll_menu);

        titleTextView = (TextView) toolbar.findViewById(R.id.row_toolbar_tv_title);
        row_tool_bar_tv_wallet_name=(TextView)toolbar.findViewById(R.id.row_toolbar_tv_user_name);

        row_tool_bar_tv_second_tab_title=(TextView)toolbar.findViewById(R.id.row_toolbar_tv_second_tab_title);
        row_tool_bar_tv_second_tab_subTitle=(TextView)toolbar.findViewById(R.id.row_toolbar_tv_second_tab_subtitle);

        menuImage = (ImageView) toolbar.findViewById(R.id.row_toolbar_iv_menu);
        wallet = (LinearLayout) toolbar.findViewById(R.id.wallet);
        search = (LinearLayout) toolbar.findViewById(R.id.search);
        calendar = (LinearLayout) toolbar.findViewById(R.id.calendar);

        menuImage.setVisibility(View.VISIBLE);
        titleTextView.setVisibility(View.GONE);
        row_tool_bar_ll_menu.setVisibility(View.VISIBLE);

        menuImage.setOnClickListener(this);
        wallet.setOnClickListener(this);
        search.setOnClickListener(this);
        calendar.setOnClickListener(this);

        final String userData = Utils.getString(this, Constant.KEY_USER_DATA);
        if (!TextUtils.isEmpty(userData) && !userData.equalsIgnoreCase("null")) {
            userResponseModel = new Gson().fromJson(userData, UserResponseModel.class);
            row_tool_bar_tv_wallet_name.setText(userResponseModel.getFirst_name().toUpperCase());
        }
    }



    public static MainActivity getInstance() {
        return instance;
    }

    public FrameLayout getRow_tool_bar_ll_menu() {
        return row_tool_bar_ll_menu;
    }

    public TextView titleTextViewVisibility() {return titleTextView;}
    public TextView secondTabTitle(){return  row_tool_bar_tv_second_tab_title;}
    public TextView secondTabSubTitle(){return  row_tool_bar_tv_second_tab_subTitle;}
    public LinearLayout walletClick(){
        return wallet;
    }
    public LinearLayout calenadrClick(){return calendar;}
    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_main;
    }
    private void displayFragment(Fragment fragment) {
        final FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, fragment.getClass().getSimpleName());
        transaction.commit();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.row_toolbar_iv_menu:
                openCloseLeftDrawer();
                break;
            case R.id.wallet:
                tabType=TabType.WALLET;
                setBackground(tabType);
                break;
            case R.id.search:
                tabType=TabType.SEARCH;
                setBackground(tabType);
                break;
            case R.id.calendar:
                tabType=TabType.CALENDAR;
                setBackground(tabType);
                break;
            case R.id.about_flunky:
                Utils.hideSoftKeyboard(this);
                openCloseLeftDrawer();
                displayFragment(new AboutFlunkyFragment());
                break;
            case R.id.my_profile:
                Utils.hideSoftKeyboard(this);
                openCloseLeftDrawer();
                displayFragment(new ProfileFragment());
                break;
            case R.id.my_home:
                Utils.hideSoftKeyboard(this);
                tabType=TabType.SEARCH;
                setBackground(tabType);
                openCloseLeftDrawer();
                displayFragment(new SearchFragment());
                break;
            case R.id.past_vouchers:
                Utils.hideSoftKeyboard(this);
                openCloseLeftDrawer();
                displayFragment(new PastVoucherFragment());
                break;
            case R.id.drawer_logout:
                showLogoutDialog();
                break;
        }
    }
    public void setBackground(TabType v) {
        switch (v) {
            case SEARCH:
                search.setBackground(getResources().getDrawable(R.drawable.ic_shadow));
                wallet.setBackgroundResource(0);
                calendar.setBackgroundResource(0);
                displayFragment(new SearchFragment());
                break;
            case WALLET:
                wallet.setBackground(getResources().getDrawable(R.drawable.ic_shadow));
                search.setBackgroundResource(0);
                calendar.setBackgroundResource(0);
                displayFragment(new WalletFragment());
                break;
            case CALENDAR:
                calendar.setBackground(getResources().getDrawable(R.drawable.ic_shadow));
               search.setBackgroundResource(0);
               wallet.setBackgroundResource(0);
                displayFragment(new CalendarFragment());
                break;


        }
    }



    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.about_flunky:
                Utils.hideSoftKeyboard(this);
                openCloseLeftDrawer();
                displayFragment(new AboutFlunkyFragment());
                break;
            case R.id.my_profile:
                Utils.hideSoftKeyboard(this);
                openCloseLeftDrawer();
                displayFragment(new ProfileFragment());
                break;
            case R.id.my_home:
                Utils.hideSoftKeyboard(this);
                search.setBackground(getResources().getDrawable(R.drawable.ic_shadow));
                wallet.setBackgroundResource(0);
                calendar.setBackgroundResource(0);
                openCloseLeftDrawer();
                displayFragment(new SearchFragment());
                break;
            case R.id.past_vouchers:
                Utils.hideSoftKeyboard(this);
                openCloseLeftDrawer();
                displayFragment(new PastVoucherFragment());
                break;
            case R.id.drawer_logout:
                showLogoutDialog();
                break;

        }
    }

    private void showLogoutDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this,R.style.AppTheme_AlertDialog);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(R.string.logout_confirmation);
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                doLogout();
                dialog.dismiss();

            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }

    private void doLogout() {
        Call<JsonObject> logout = new RestClient().getApiInterface().logoutCall(Constant.APP_KEY,Utils.getString(this,Constant.User_id),Utils.getUniqueDeviceId(this),Utils.getString(this,Constant.GCM_UNIQUE_ID),Constant.OS);
        logout.enqueue(new RetrofitCallback<JsonObject>(this,Logger.showProgressDialog(this)) {
            @Override
            public void onSuccess(JsonObject data) {
                Logger.toast(MainActivity.this,data.get("res_message").toString());
                if(data.get("res_code").getAsString().equalsIgnoreCase("0")){

                }else{
                    Utils.storeString(MainActivity.this,Constant.User_id,"");
                    final Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                    navigateToNextActivity(intent, true);
                }

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if(row_tool_bar_tv_second_tab_title.getText().toString().equalsIgnoreCase(getString(R.string.check_out_capital))){
            row_tool_bar_ll_menu.setBackgroundColor(getResources().getColor(R.color.black));
            row_tool_bar_tv_second_tab_subTitle.setText(R.string.flunky_feed);
            wallet.setOnClickListener(this);
            calendar.setOnClickListener(this);
        }
    }
}






























