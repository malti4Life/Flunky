package com.flunky.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.flunky.Model.RestaurantSortMenuItemsModel;
import com.flunky.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.gson.Gson;

import static com.facebook.FacebookSdk.getApplicationContext;


public class Utils {


    /**
     * Check that network mobile data or 3G is available or not.
     *
     * @param context
     * @return boolean
     */
    public static boolean DEBUG = false;

    public static final boolean isConnectedToInternet(Context context) {
        if (context != null) {
            final ConnectivityManager mgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (mgr != null) {

                final NetworkInfo mobileInfo = mgr.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
                final NetworkInfo wifiInfo = mgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if (wifiInfo != null && wifiInfo.isAvailable() && wifiInfo.isAvailable() && wifiInfo.isConnected()) {

                    final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                    final WifiInfo wifiInfoStatus = wifiManager.getConnectionInfo();
                    final SupplicantState supState = wifiInfoStatus.getSupplicantState();

                    if (String.valueOf(supState).equalsIgnoreCase("COMPLETED") || String.valueOf(supState).equalsIgnoreCase("ASSOCIATED")) {
                        // WiFi is connected
                        return true;
                    }
                }

                if (mobileInfo != null && mobileInfo.isAvailable() && mobileInfo.isConnected()) {
                    // Mobile Network is connected
                    return true;
                }
            }
        }
        return false;
    }


    /**
     * Check email is valid or not.
     *
     * @param email
     * @return boolean
     */
    public static final boolean isEmailValid(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    /**
     * Check emailclient is installed or not.
     *
     * @return boolean
     */
    public static boolean isMailClientPresent(Context context) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/html");
        final PackageManager packageManager = context.getPackageManager();
        List<ResolveInfo> list = packageManager.queryIntentActivities(intent, 0);

        if (list.size() == 0)
            return false;
        else
            return true;
    }

    /**
     * Store string value in shared preference.
     *
     * @param context
     * @param key
     * @param value
     */
    public static final void storeString(Context context, String key, String value) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }


    /**
     * Get string value from shared preference.
     *
     * @param context
     * @param key
     * @return String
     */
    public static final String getString(Context context, String key) {
        String data;
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final SharedPreferences.Editor editor = preferences.edit();
        data = preferences.getString(key, "");
        editor.commit();
        return data;
    }


    /**
     * Store arraylist value from shared preference.
     *
     * @param context
     * @param key
     * @param restaurantSortMenuItemsModels
     */
    public static final void storeArrayList(Context context, String key, ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuItemsModels) {

        if (!restaurantSortMenuItemsModels.isEmpty()) {
            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
            final SharedPreferences.Editor editor = preferences.edit();
            Gson gson = new Gson();
            String jsonFavorites = gson.toJson(restaurantSortMenuItemsModels);
            editor.putString(key, jsonFavorites);
            editor.commit();
        }


    }

    public static final ArrayList getArrayList(Context context, String key) {
        ArrayList favorites;
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences.contains(key)) {
            String jsonFavorites = preferences.getString(key, null);
            Gson gson = new Gson();
            RestaurantSortMenuItemsModel[] favoriteItems = gson.fromJson(jsonFavorites, RestaurantSortMenuItemsModel[].class);
            favorites = new ArrayList<>(Arrays.asList(favoriteItems));
            return favorites;
        } else
            return null;

    }


    /**
     * Hide keyboard if it is visible
     *
     * @param context
     */
    public static final void hideSoftKeyboard(Context context) {
        final InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager.isActive()) {
            if (((Activity) context).getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus().getWindowToken(), 0);
            }
        }
    }

    /**
     * rupee formatter.
     *
     * @param value
     * @return value
     */
    public static String rupeeFormat(String value) {
        DecimalFormat formatter = new DecimalFormat("#.00");
        if (!TextUtils.isEmpty(value))
            return formatter.format(Double.valueOf(value));
        else return formatter.format(Double.valueOf("0"));
    }

    /**
     * Device is tablet or not.
     *
     * @param context
     * @return boolean
     */

    public static final String getUniqueDeviceId(Context context) {

        String deviceId = null;
        // 1 compute IMEI
        TelephonyManager TelephonyMgr = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        deviceId = TelephonyMgr.getDeviceId(); // Requires // READ_PHONE_STATE

        if (deviceId == null) {
            // 2 android ID - unreliable
            deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        if (deviceId == null) {
            // 3 compute DEVICE ID
            deviceId = "35"
                    + // we make this look like a valid IMEI
                    Build.BOARD.length() % 10 + Build.BRAND.length() % 10 + Build.DEVICE.length() % 10 + Build.DISPLAY.length() % 10 + Build.HOST.length() % 10 + Build.ID.length() % 10 + Build.MANUFACTURER.length() % 10 + Build.MODEL.length() % 10
                    + Build.PRODUCT.length() % 10 + Build.TAGS.length() % 10 + Build.TYPE.length() % 10 + Build.USER.length() % 10; // 13
            // digits
        }
        return deviceId;
    }


    public static boolean checkPlayServices(Activity activity) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(activity, resultCode, Constant.PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Logger.i("This device is not supported.");
                activity.finish();
            }
            return false;
        }
        return true;
    }

    public static int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.mipmap.ic_launcher : R.mipmap.ic_launcher;
    }

    public static String getUpperCaseString(final String string) {
        if(string.length()!=0){
            char[] stringArrayy = string.trim().toCharArray();
            if (Character.isLowerCase(stringArrayy[0])) {
                return string.substring(0, 1).toUpperCase() + string.substring(1);
            } else {
                return string;
            }
        }
       return string;
    }


}
