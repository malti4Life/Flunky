package com.flunky.Model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RestaurantMenuResponseModel implements Parcelable {
    @SerializedName("title")
    private String title;

    @SerializedName("item")
    private ArrayList<RestaurantRecommendationResponseModel>restaurantRecommendationResponseModels;

    protected RestaurantMenuResponseModel(Parcel in) {
        title = in.readString();
        restaurantRecommendationResponseModels = (ArrayList<RestaurantRecommendationResponseModel>) in.readSerializable();
    }

    public static final Creator<RestaurantMenuResponseModel> CREATOR = new Creator<RestaurantMenuResponseModel>() {
        @Override
        public RestaurantMenuResponseModel createFromParcel(Parcel in) {
            return new RestaurantMenuResponseModel(in);
        }

        @Override
        public RestaurantMenuResponseModel[] newArray(int size) {
            return new RestaurantMenuResponseModel[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<RestaurantRecommendationResponseModel> getRestaurantRecommendationResponseModels() {
        return restaurantRecommendationResponseModels;
    }

    public void setRestaurantRecommendationResponseModels(ArrayList<RestaurantRecommendationResponseModel> restaurantRecommendationResponseModels) {
        this.restaurantRecommendationResponseModels = restaurantRecommendationResponseModels;
    }

    @Override
    public int describeContents() {

        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags)
    {
        dest.writeString(title);
        dest.writeSerializable(restaurantRecommendationResponseModels);
    }
}
