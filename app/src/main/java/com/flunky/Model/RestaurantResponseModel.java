package com.flunky.Model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RestaurantResponseModel {
    private boolean has_more_clicked=false;


    @SerializedName("account_id")
    private String user_account_id;

    @SerializedName("rest_id")
    private String rest_id;

    @SerializedName("rest_title")
    private String rest_title;

    @SerializedName("rest_adress")
    private String rest_address;

    @SerializedName("rest_img")
    private String rest_img;

    @SerializedName("rest_cost")
    private String rest_cost;

    @SerializedName("rest_cuisine")
    private String rest_cuisine;

    @SerializedName("is_wishlist")
    private String is_wishlist;

    @SerializedName("is_exlclusive")
    private String is_exclusive;

    @SerializedName("rest_recommends")
    private ArrayList<RestaurantRecommendationResponseModel> restaurantRecommendationResponseModel;

    @SerializedName("rest_menu")
    private ArrayList<RestaurantMenuResponseModel> restaurantMenuResponseModels;

    @SerializedName("has_more")
    private int has_more;


    public String getUser_account_id() {
        return user_account_id;
    }

    public void setUser_account_id(String user_account_id) {
        this.user_account_id = user_account_id;
    }
    public boolean isHas_more_clicked() {
        return has_more_clicked;
    }

    public void setHas_more_clicked(boolean has_more_clicked) {
        this.has_more_clicked = has_more_clicked;
    }
    public String getRest_id() {
        return rest_id;
    }

    public void setRest_id(String rest_id) {
        this.rest_id = rest_id;
    }

    public String getRest_title() {
        return rest_title;
    }

    public void setRest_title(String rest_title) {
        this.rest_title = rest_title;
    }

    public String getRest_address() {
        return rest_address;
    }

    public void setRest_address(String rest_address) {
        this.rest_address = rest_address;
    }

    public String getRest_img() {
        return rest_img;
    }

    public void setRest_img(String rest_img) {
        this.rest_img = rest_img;
    }

    public String getRest_cost() {
        return rest_cost;
    }

    public void setRest_cost(String rest_cost) {
        this.rest_cost = rest_cost;
    }

    public String getRest_cuisine() {
        return rest_cuisine;
    }

    public void setRest_cuisine(String rest_cuisine) {
        this.rest_cuisine = rest_cuisine;
    }

    public String is_wishlist() {
        return is_wishlist;
    }

    public void setIs_wishlist(String is_wishlist) {
        this.is_wishlist = is_wishlist;
    }

    public String is_exclusive() {
        return is_exclusive;
    }

    public void setIs_exclusive(String is_exclusive) {
        this.is_exclusive = is_exclusive;
    }

    public ArrayList<RestaurantRecommendationResponseModel> getRestaurantRecommendationResponseModel() {
        return restaurantRecommendationResponseModel;
    }

    public void setRestaurantRecommendationResponseModel(ArrayList<RestaurantRecommendationResponseModel> restaurantRecommendationResponseModel) {
        this.restaurantRecommendationResponseModel = restaurantRecommendationResponseModel;
    }

    public ArrayList<RestaurantMenuResponseModel> getRestaurantMenuResponseModels() {
        return restaurantMenuResponseModels;
    }

    public void setRestaurantMenuResponseModels(ArrayList<RestaurantMenuResponseModel> restaurantMenuResponseModels) {
        this.restaurantMenuResponseModels = restaurantMenuResponseModels;
    }

    public int isHas_more() {
        return has_more;
    }

    public void setHas_more(int has_more) {
        this.has_more = has_more;
    }
}
