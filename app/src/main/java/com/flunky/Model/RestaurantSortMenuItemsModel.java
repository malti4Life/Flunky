package com.flunky.Model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class RestaurantSortMenuItemsModel implements Parcelable {
    @SerializedName("id")
    private String id;

    @SerializedName("value")
    private String value;

    private boolean is_checked=false;

    protected RestaurantSortMenuItemsModel(Parcel in) {
        id = in.readString();
        value = in.readString();
        is_checked = in.readByte() != 0;
    }

    public static final Creator<RestaurantSortMenuItemsModel> CREATOR = new Creator<RestaurantSortMenuItemsModel>() {
        @Override
        public RestaurantSortMenuItemsModel createFromParcel(Parcel in) {
            return new RestaurantSortMenuItemsModel(in);
        }

        @Override
        public RestaurantSortMenuItemsModel[] newArray(int size) {
            return new RestaurantSortMenuItemsModel[size];
        }
    };

    public boolean is_checked() {
        return is_checked;
    }

    public void setIs_checked(boolean is_checked) {
        this.is_checked = is_checked;
    }




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(value);
        dest.writeByte((byte) (is_checked ? 1 : 0));
    }
}
