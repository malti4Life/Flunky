package com.flunky.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ResponseOfAllApiModel {
    @SerializedName("user_details")
    private UserResponseModel userResponseModel;

    @SerializedName("rest_details")
    private ArrayList<RestaurantResponseModel> restaurantResponseModel;

    @SerializedName("rest_sortmenu")
    private RestaurantSortMenuResponseModel restaurantSortMenuResponseModel;

    @SerializedName("wallet_details")
    private ArrayList<WalletDetailsModel> walletDetailsModelArrayList;
    @SerializedName("event_calendar")
    private ArrayList<EventDataModel> eventDataModelArrayList;

    public ArrayList<EventDataModel> getEventDataModelArrayList() {
        return eventDataModelArrayList;
    }

    public void setEventDataModelArrayList(ArrayList<EventDataModel> eventDataModelArrayList) {
        this.eventDataModelArrayList = eventDataModelArrayList;
    }

    public ArrayList<WalletDetailsModel> getWalletDetailsModelArrayList() {
        return walletDetailsModelArrayList;
    }

    public void setWalletDetailsModelArrayList(ArrayList<WalletDetailsModel> walletDetailsModelArrayList) {
        this.walletDetailsModelArrayList = walletDetailsModelArrayList;
    }


    public RestaurantSortMenuResponseModel getRestaurantSortMenuResponseModel() {
        return restaurantSortMenuResponseModel;
    }

    public void setRestaurantSortMenuResponseModel(RestaurantSortMenuResponseModel restaurantSortMenuResponseModel) {
        this.restaurantSortMenuResponseModel = restaurantSortMenuResponseModel;
    }



    public ArrayList<RestaurantResponseModel> getRestaurantResponseModel() {
        return restaurantResponseModel;
    }

    public void setRestaurantResponseModel(ArrayList<RestaurantResponseModel> restaurantResponseModel) {
        this.restaurantResponseModel = restaurantResponseModel;
    }


    public UserResponseModel getUserResponseModel() {
        return userResponseModel;
    }

    public void setUserResponseModel(UserResponseModel userResponseModel) {
        this.userResponseModel = userResponseModel;
    }


}
