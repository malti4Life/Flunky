package com.flunky.Model;

import com.google.gson.annotations.SerializedName;

public class WalletDetailsModel {
    private boolean has_more_clicked = false;

    @SerializedName("rest_id")
    private String rest_id;

    @SerializedName("rest_title")
    private String rest_title;

    @SerializedName("rest_adress")
    private String rest_address;

    @SerializedName("rest_img")
    private String rest_img;

    @SerializedName("image_one")
    private String image_one;

    @SerializedName("occasion")
    private String occasion;

    @SerializedName("balance_amount")
    private String balance_amount;

    @SerializedName("redeem_cofe")
    private String redeem_cofe;



    @SerializedName("redeem_qr_code")
    private String reedem_qr_code;

    @SerializedName("expiry_date")
    private String expiry_date;

    @SerializedName("sender_name")
    private String sender_name;

    @SerializedName("personal_msg")
    private String personal_msg;

    public String getReedem_qr_code() {
        return reedem_qr_code;
    }

    public void setReedem_qr_code(String reedem_qr_code) {
        this.reedem_qr_code = reedem_qr_code;
    }
    public boolean isHas_more_clicked() {
        return has_more_clicked;
    }

    public void setHas_more_clicked(boolean has_more_clicked) {
        this.has_more_clicked = has_more_clicked;
    }

    public String getRest_id() {
        return rest_id;
    }

    public void setRest_id(String rest_id) {
        this.rest_id = rest_id;
    }

    public String getRest_title() {
        return rest_title;
    }

    public void setRest_title(String rest_title) {
        this.rest_title = rest_title;
    }

    public String getRest_address() {
        return rest_address;
    }

    public void setRest_address(String rest_address) {
        this.rest_address = rest_address;
    }

    public String getRest_img() {
        return rest_img;
    }

    public void setRest_img(String rest_img) {
        this.rest_img = rest_img;
    }

    public String getImage_one() {
        return image_one;
    }

    public void setImage_one(String image_one) {
        this.image_one = image_one;
    }

    public String getOccasion() {
        return occasion;
    }

    public void setOccasion(String occasion) {
        this.occasion = occasion;
    }

    public String getBalance_amount() {
        return balance_amount;
    }

    public void setBalance_amount(String balance_amount) {
        this.balance_amount = balance_amount;
    }

    public String getRedeem_cofe() {
        return redeem_cofe;
    }

    public void setRedeem_cofe(String redeem_cofe) {
        this.redeem_cofe = redeem_cofe;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getSender_name() {
        return sender_name;
    }

    public void setSender_name(String sender_name) {
        this.sender_name = sender_name;
    }

    public String getPersonal_msg() {
        return personal_msg;
    }

    public void setPersonal_msg(String personal_msg) {
        this.personal_msg = personal_msg;
    }
}
