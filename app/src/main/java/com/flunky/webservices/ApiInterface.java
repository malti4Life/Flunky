package com.flunky.webservices;


import com.flunky.Model.EventDataModel;
import com.flunky.Model.ResponseOfAllApiModel;
import com.google.gson.JsonObject;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface ApiInterface {
    @FormUrlEncoded
    @POST("index.php/appregister")
    Call<JsonObject> doSignUp(@Field("email") String email, @Field("first_name")String displayname,
                              @Field("last_name")String lastName,@Field("birth_date")String birthday, @Field("mobile")String mobile, @Field("password")String pwd,
                              @Field("pass_confirm")String cnf_pwd,
                              @Field("apikey")String key, @Field("useraccounttype")String userType, @Field("facebookid")String facebookId,@Field("os")String os);
    @FormUrlEncoded
    @POST("index.php/applogin")
    Call<ResponseOfAllApiModel>doLogin(@Field("login")String email, @Field("facebookid")String facebbok_id, @Field("password")String password,
                                       @Field("useraccounttype")int userAccountType, @Field("apikey")String apiKey, @Field("deviceid")String device_id,
                                       @Field("devicetoken")String device_token,@Field("os")String os);
    @FormUrlEncoded
    @POST("index.php/apprestaurant")
    Call<ResponseOfAllApiModel>getRestaurantData(
            @Field("apikey")String apiKey, @Field("user_id")String user_id,
            @Field("latitude")String lat, @Field("longitude") String lang, @Field("rest_name") String rest_name, @Field("rest_cuisine")String rest_cusine,
            @Field("rest_locations")String rest_locations, @Field("rest_cost") String rest_cost, @Field("is_wishlist")String wishlist, @Field("page_no") String page_no,@Field("os")String os);

    @FormUrlEncoded
    @POST("index.php/appsortmenu")
    Call<ResponseOfAllApiModel>getRestaurantSortMenu(@Field("apikey")String apiKey,@Field("os")String os);

    @FormUrlEncoded
    @POST("index.php/addwishlist")
    Call<JsonObject>removeItemFromWishlist(@Field("apikey")String apiKey,@Field("user_id") String user_id,
                                           @Field("action")String action,@Field("rest_id") String rest_id,@Field("os")String os);

    @Multipart
    @POST("index.php/getcheckout")
    Call<JsonObject> getCheckout(@Part MultipartBody.Part image_one,@Part("apikey") RequestBody apiKey,@Part("user_id") RequestBody user_id,
                                 @Part("receiver_name")RequestBody receiver_name, @Part("receiver_phone")RequestBody phone,
                                 @Part("occasion_id") RequestBody occassion_id,@Part("occasion_date") RequestBody occasion_date,
                                 @Part("send_date") RequestBody send_date,@Part("is_now") RequestBody is_now,
                                 @Part("peronal_msg") RequestBody personal_message,
                                 @Part("gift_amount_id")RequestBody gift_amount_id,@Part("stripeAmount") RequestBody stripeAmount,@Part("stripeCustomerID")RequestBody stripeRestaurantAccId,
                                 @Part("stripeToken") RequestBody stripeToken,@Part("rest_id")RequestBody rest_id,@Part("os")RequestBody os);
    @FormUrlEncoded
    @POST("index.php/getwallet")
    Call<ResponseOfAllApiModel>getWalletDetails(@Field("apikey")String apiKey,@Field("user_id")String user_id,@Field("os")String os);

    @FormUrlEncoded
    @POST("index.php/getallvouchers")
    Call<ResponseOfAllApiModel>getVoucherDetails(@Field("apikey")String apiKey,@Field("user_id")String user_id,@Field("is_action")String is_action,@Field("os")String os);

    @FormUrlEncoded
    @POST("index.php/updateuserprofile")
    Call<ResponseOfAllApiModel> updateProfile(@Field("apikey")String apiKey,@Field("user_id")String user_id,@Field("first_name")String first_name
            ,@Field("last_name")String last_name,@Field("birth_date")String birthDate,@Field("os")String os);

    @FormUrlEncoded
    @POST("index.php/changeuserpass")
    Call<JsonObject> changePassword(@Field("apikey")String apikey,@Field("user_id")String user_id,@Field("password")String password,@Field("os")String os);

    @FormUrlEncoded
    @POST("index.php/appforgotpwd")
    Call<JsonObject>forgotPwdCall(@Field("apikey")String apikey,@Field("email")String email,@Field("os")String os);

    @FormUrlEncoded
    @POST("index.php/userlogout")
    Call<JsonObject>logoutCall(@Field("apikey")String apikey,@Field("user_id")String userId,@Field("deviceid")String deviceId,@Field("devicetocken")String deviceToken,@Field("os")String os);

    @FormUrlEncoded
    @POST("index.php/addcalendar")
    Call<JsonObject> addEvent(@Field("apikey")String apikey,@Field("user_id")String userId,@Field("event_name")String event_name,@Field("event_occasion")String event_occasion,
                              @Field("event_desc")String event_desc,@Field("event_date") String event_date,@Field("notify")String notify,@Field("is_repeat")String isRepeate);
    @FormUrlEncoded
    @POST("index.php/getcalendar")
    Call<ResponseOfAllApiModel>getEventDetail(@Field("apikey")String apikey, @Field("user_id")String userId, @Field("e_month")String event_month, @Field("e_year")String event_year);

    @FormUrlEncoded
    @POST("index.php/removecalendar")
    Call<JsonObject>removeEvent(@Field("apikey")String apikey, @Field("event_id")String eventId);



}