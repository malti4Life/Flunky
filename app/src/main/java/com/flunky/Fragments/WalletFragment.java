package com.flunky.Fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flunky.Activities.MainActivity;
import com.flunky.Adapters.WalletRecyclerViewAdapter;
import com.flunky.Interfaces.OnRecyclerViewItemClickListener;
import com.flunky.Model.ResponseOfAllApiModel;
import com.flunky.Model.WalletDetailsModel;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;

import java.util.ArrayList;

import retrofit2.Call;


public class WalletFragment extends BaseFragment implements OnRecyclerViewItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout walletFragment_sl;
    private RecyclerView walletFragment_rv;
    private WalletRecyclerViewAdapter walletRecyclerViewAdapter;
    private ArrayList<WalletDetailsModel>walletDetailsModelArrayList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_wallet, container, false);
    }

    @Override
    protected void initView(View view) {
        walletDetailsModelArrayList=new ArrayList<>();
        walletFragment_sl=(SwipeRefreshLayout)view.findViewById(R.id.fragment_wallet_sl);
        walletFragment_rv = (RecyclerView) view.findViewById(R.id.fragment_wallet_rv);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        walletFragment_rv.setLayoutManager(linearLayoutManager);
        walletRecyclerViewAdapter = new WalletRecyclerViewAdapter(getContext(),walletDetailsModelArrayList);
        walletRecyclerViewAdapter.setOnRecyclerViewItemClickListener(this);
        walletFragment_rv.setAdapter(walletRecyclerViewAdapter);

        walletFragment_sl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        walletFragment_sl.setOnRefreshListener(this);
        getWalletDetail();

    }

    private void getWalletDetail() {
        Call<ResponseOfAllApiModel> getWalletData=new RestClient().getApiInterface().getWalletDetails(Constant.APP_KEY,Utils.getString(getContext(),Constant.User_id),Constant.OS);
        getWalletData.enqueue(new RetrofitCallback<ResponseOfAllApiModel>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApiModel data) {
                if(!data.getWalletDetailsModelArrayList().isEmpty()){
                    walletDetailsModelArrayList.trimToSize();
                    walletDetailsModelArrayList.addAll(data.getWalletDetailsModelArrayList());
                    walletRecyclerViewAdapter.notifyDataSetChanged();
                }else{
                    Logger.toast(getContext(),"No Data Available");
                }
            }
            @Override
            public void onFailure(Call<ResponseOfAllApiModel> call, Throwable error) {
                super.onFailure(call, error);
            }
        });
    }

    @Override
    protected void initToolbar() {
       MainActivity.getInstance().getRow_tool_bar_ll_menu().setBackgroundColor(getResources().getColor(R.color.appColor));
    }


    @Override
    public void onItemClick(int position, View view) {
        final WalletDetailsModel walletDetailsModel ;
        switch (view.getId()){
            case R.id.row_fragment_wallet_iv_more:
                walletDetailsModel=(WalletDetailsModel) view.getTag();
                walletDetailsModel.setHas_more_clicked(true);
                walletDetailsModelArrayList.remove(position);
                walletDetailsModelArrayList.add(position,walletDetailsModel);
                walletRecyclerViewAdapter.notifyDataSetChanged();
                break;
            case R.id.row_fragment_wallet_iv_more_close:
                walletDetailsModel=(WalletDetailsModel) view.getTag();
                walletDetailsModel.setHas_more_clicked(false);
                walletDetailsModelArrayList.remove(position);
                walletDetailsModelArrayList.add(position,walletDetailsModel);
                walletRecyclerViewAdapter.notifyDataSetChanged();
                break;
            case R.id.row_fragment_wallet_tv_book_now:
                String url = "https://goo.gl/";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
        }
    }

    @Override
    public void onRefresh() {
        walletDetailsModelArrayList.clear();
        getWalletDetail();
        walletFragment_sl.setRefreshing(false);
    }
}

