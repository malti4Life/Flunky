package com.flunky.Fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.flunky.Activities.MainActivity;
import com.flunky.R;
import com.flunky.imagepicker.ImagePicker;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.TabType;
import com.flunky.utils.Utils;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;
import com.google.gson.JsonObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;

public class AddEventFragment extends BaseFragment implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private EditText fragment_addEvent_et_title;
    private TextView fragment_addEvent_tv_date;
    private EditText fragment_addEvent_et_notes;
    private RadioGroup fragment_addEvent_rg_alert;
    private TextView fragment_addEvent_cb_repeat_yearly;

    private TextView addEvent_fragment_tv_add;
    
    private SimpleDateFormat dateSimpleDateFormat;
    private Calendar dateTimeCalendar;
    private Calendar todayCalendar;
   
    private String alert="alert";
    private boolean isRepeated=false;
    private String repeate="0";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_add_event,container,false);
    }
    @Override
    protected void initView(View view) {
        
        dateTimeCalendar = Calendar.getInstance();
        todayCalendar = Calendar.getInstance();
        dateSimpleDateFormat = new SimpleDateFormat(Constant.EXPECTED_DATE_FORMATTER, Locale.getDefault());
        
        fragment_addEvent_et_title=(EditText)view.findViewById(R.id.fragment_add_event_et_title);       
        fragment_addEvent_et_notes=(EditText)view.findViewById(R.id.fragment_add_event_et_notes);

        fragment_addEvent_rg_alert=(RadioGroup)view.findViewById(R.id.fragment_addEvent_rg_alert);
        fragment_addEvent_cb_repeat_yearly=(TextView) view.findViewById(R.id.fragment_add_event_cb_repeat);
        fragment_addEvent_tv_date=(TextView)view.findViewById(R.id.fragment_add_event_tv_date);
        addEvent_fragment_tv_add=(TextView)view.findViewById(R.id.fragment_addEvent_tv_add);
        
        addEvent_fragment_tv_add.setOnClickListener(this);
        fragment_addEvent_tv_date.setOnClickListener(this);
        fragment_addEvent_rg_alert.setOnCheckedChangeListener(this);
        fragment_addEvent_cb_repeat_yearly.setOnClickListener(this);
    }

    @Override
    protected void initToolbar() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_addEvent_tv_add:
                validateData();
                break;
            case R.id.fragment_add_event_tv_date:
                new DatePickerDialog(getContext(), onOccasionDateSetListener, dateTimeCalendar.get(Calendar.YEAR),
                        dateTimeCalendar.get(Calendar.MONTH), dateTimeCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.fragment_add_event_cb_repeat:
                isRepeated=!isRepeated;
                if(isRepeated){
                    repeate="1";
                    fragment_addEvent_cb_repeat_yearly.setCompoundDrawablesRelativeWithIntrinsicBounds( R.drawable.ic_selected_checkbox, 0, 0, 0);
                }else{
                    repeate="0";
                    fragment_addEvent_cb_repeat_yearly.setCompoundDrawablesRelativeWithIntrinsicBounds( R.drawable.ic_unselected_checkbox, 0, 0, 0);
                }
                break;
        }
    }

    private void validateData() {
        final String title = fragment_addEvent_et_title.getText().toString();
        final String date = fragment_addEvent_tv_date.getText().toString();
        final String note = fragment_addEvent_et_notes.getText().toString();
        if(!TextUtils.isEmpty(title)){
         if(!TextUtils.isEmpty(date)){
            if(!alert.equalsIgnoreCase("alert")){
               if(!TextUtils.isEmpty(note)){
                   addEvent(title,date,note);
                   Log.e("myDate",date);
               }else{
                   Logger.showSnackBar(getContext(),getString(R.string.enter_note));
               }
            }else{
                Logger.showSnackBar(getContext(),getString(R.string.select_alert_option));
            }
         }else{
            Logger.showSnackBar(getContext(),getString(R.string.select_date));
         }
        }else{
           Logger.showSnackBar(getContext(),getString(R.string.enter_title));
        }
    }

    private void addEvent(String title, final String date, String note) {
        Call<JsonObject> addEvent=new RestClient().getApiInterface().addEvent(Constant.APP_KEY, Utils.getString(getContext(),Constant.User_id),title,"event_occ",note,date,alert,repeate);
        addEvent.enqueue(new RetrofitCallback<JsonObject>(getContext(),Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get("res_code").getAsString().equalsIgnoreCase("1")){
                    Logger.toast(getContext(),data.get("res_message").toString());
                    MainActivity.getInstance().setBackground(TabType.CALENDAR);
                }
            }
        });
    }

    private DatePickerDialog.OnDateSetListener onOccasionDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            dateTimeCalendar.set(Calendar.YEAR, year);
            dateTimeCalendar.set(Calendar.MONTH, month);
            dateTimeCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            if (dateTimeCalendar.compareTo(todayCalendar) < 0) {
                Logger.showSnackBar(getContext(), "Your not allowed to select less than today's date");
            } else {
                fragment_addEvent_tv_date.setText(dateSimpleDateFormat.format(dateTimeCalendar.getTime()));
            }
        }
    };

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.fragment_add_event_rb_yes:
                alert="1";
                break;
            case R.id.fragment_add_event_rb_no:
                alert="0";
                break;

                
        }
    }
}
