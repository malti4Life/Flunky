package com.flunky.Fragments;

import android.app.FragmentManager;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flunky.Activities.MainActivity;
import com.flunky.Adapters.RestaurantSortMenuItemsAdapter;
import com.flunky.Adapters.SearchRecyclerViewAdapter;
import com.flunky.FlunkyApplication;
import com.flunky.Interfaces.OnItemCheckChangeListener;
import com.flunky.Interfaces.OnRecyclerViewItemClickListener;
import com.flunky.Model.RestaurantMenuResponseModel;
import com.flunky.Model.RestaurantResponseModel;
import com.flunky.Model.ResponseOfAllApiModel;
import com.flunky.Model.RestaurantSortMenuItemsModel;
import com.flunky.Model.UserResponseModel;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.TabType;
import com.flunky.utils.Utils;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import java.util.ArrayList;


import retrofit2.Call;


public class SearchFragment extends BaseFragment implements View.OnClickListener, OnRecyclerViewItemClickListener, OnItemCheckChangeListener, SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout searchFragment_sl;
    private LinearLayout searchFragment_ll_tabs_location;
    private LinearLayout searchFragment_ll_tabs_wishlist;
    private LinearLayout searchFragment_ll_tabs_cusine;
    private LinearLayout searchFragment_ll_tabs_price;
    private LinearLayout searchFragment_ll_tabs_view;

    private TextView search_fragment_tv_search_location;
    private TextView search_fragment_tv_search_wishlist;
    private TextView search_fragment_tv_search_cusine;
    private TextView search_fragment_tv_search_price;

    private ImageView search_fragment_iv_search_location;
    private ImageView search_fragment_iv_search_wishlist;
    private ImageView search_fragment_iv_search_cusine;
    private ImageView search_fragment_iv_search_price;

    private ImageView search_fragment_iv_close_tab;


    private LinearLayout search_fragment_ll_search_location_view;
    private LinearLayout searchFragment_ll_search_cusine_view;
    private LinearLayout searchFragment_ll_search_price_view;
    private LinearLayout searchFragment_ll_submit;

    private RecyclerView rv_search;
    private RecyclerView search_fragment_tabs_rv_location;
    private RecyclerView search_fragment_tabs_rv_cusine;
    private RecyclerView search_fragment_tabs_rv_price;

    private SearchRecyclerViewAdapter search_rv_ad;
    private RestaurantSortMenuItemsAdapter search_fragment_restaurantSortMenu_ad;


    private UserResponseModel userResponseModel;
    private ArrayList<RestaurantResponseModel> restaurantResponseModels;
    private ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuLocationModel;
    private ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuCusineModel;
    private ArrayList<RestaurantSortMenuItemsModel> restaurantSortMenuPriceModel;

    private LinearLayoutManager linearLayoutManager;

    private TabType tabType;
    private ArrayList<RestaurantSortMenuItemsModel> locationAddList;
    private ArrayList<RestaurantSortMenuItemsModel> cusineAddList;
    private ArrayList<RestaurantSortMenuItemsModel> priceAddList;
    private StringBuilder location;
    private StringBuilder cusine;
    private StringBuilder price;
    private String wishlist = "0";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onRefresh() {
        restaurantResponseModels.clear();
        restaurantSortMenuLocationModel.clear();
        restaurantSortMenuCusineModel.clear();
        restaurantSortMenuPriceModel.clear();

        locationAddList.clear();
        cusineAddList.clear();
        priceAddList.clear();
        location.setLength(0);
        cusine.setLength(0);
        price.setLength(0);
        wishlist = "0";
        clearAllTabs();
        getRestaurantDetail(userResponseModel.getUser_id(), Constant.APP_KEY, cusine.toString(), location.toString(), price.toString());
        getRestaurantSortMenuItems(Constant.APP_KEY);
        searchFragment_sl.setRefreshing(false);
    }

    @Override
    protected void initView(View view) {
        locationAddList = new ArrayList<>();
        cusineAddList = new ArrayList<>();
        priceAddList = new ArrayList<>();
        restaurantResponseModels = new ArrayList<>();
        location = new StringBuilder();
        cusine = new StringBuilder();
        price = new StringBuilder();

        linearLayoutManager = new LinearLayoutManager(getContext());
        final LinearLayoutManager locationLayoutManager = new LinearLayoutManager(getContext());
        final LinearLayoutManager cusineLayoutManager = new LinearLayoutManager(getContext());
        final LinearLayoutManager priceLayoutManager = new LinearLayoutManager(getContext());

        searchFragment_sl = (SwipeRefreshLayout) view.findViewById(R.id.fragment_search_swipeLayout);

        searchFragment_ll_tabs_location = (LinearLayout) view.findViewById(R.id.fragment_search_ll_location);
        searchFragment_ll_tabs_wishlist = (LinearLayout) view.findViewById(R.id.fragment_search_ll_wishlist);
        searchFragment_ll_tabs_cusine = (LinearLayout) view.findViewById(R.id.fragment_search_ll_cusine);
        searchFragment_ll_tabs_price = (LinearLayout) view.findViewById(R.id.fragment_search_ll_price);
        searchFragment_ll_tabs_view = (LinearLayout) view.findViewById(R.id.fragment_search_ll_tabs_view);

        search_fragment_tv_search_location = (TextView) view.findViewById(R.id.fragment_search_tv_location);
        search_fragment_tv_search_cusine = (TextView) view.findViewById(R.id.fragment_search_tv_cusine);
        search_fragment_tv_search_wishlist = (TextView) view.findViewById(R.id.fragment_search_tv_wishlist);
        search_fragment_tv_search_price = (TextView) view.findViewById(R.id.fragment_search_tv_price);

        search_fragment_iv_search_location = (ImageView) view.findViewById(R.id.fragment_search_iv_location);
        search_fragment_iv_search_cusine = (ImageView) view.findViewById(R.id.fragment_search_iv_cusine);
        search_fragment_iv_search_wishlist = (ImageView) view.findViewById(R.id.fragment_search_iv_wishlist);
        search_fragment_iv_search_price = (ImageView) view.findViewById(R.id.fragment_search_iv_price);

        search_fragment_iv_close_tab = (ImageView) view.findViewById(R.id.fragment_search_iv_close_tab);


        search_fragment_ll_search_location_view = (LinearLayout) view.findViewById(R.id.fragment_search_ll_location_view);
        searchFragment_ll_search_cusine_view = (LinearLayout) searchFragment_ll_tabs_view.findViewById(R.id.fragment_search_ll_cusine_view);
        searchFragment_ll_search_price_view = (LinearLayout) searchFragment_ll_tabs_view.findViewById(R.id.fragment_search_ll_price_view);
        searchFragment_ll_submit = (LinearLayout) searchFragment_ll_tabs_view.findViewById(R.id.fragment_search_ll_submit);


        rv_search = (RecyclerView) view.findViewById(R.id.fragment_search_rv);
        rv_search.setLayoutManager(linearLayoutManager);
        search_rv_ad = new SearchRecyclerViewAdapter(getContext(), restaurantResponseModels);
        rv_search.setAdapter(search_rv_ad);

        search_fragment_tabs_rv_location = (RecyclerView) view.findViewById(R.id.fragment_search_tabs_rv_location);
        search_fragment_tabs_rv_cusine = (RecyclerView) view.findViewById(R.id.fragment_search_tabs_rv_cusine);
        search_fragment_tabs_rv_price = (RecyclerView) view.findViewById(R.id.fragment_search_tabs_rv_price);

        search_fragment_tabs_rv_location.setLayoutManager(locationLayoutManager);
        search_fragment_tabs_rv_cusine.setLayoutManager(cusineLayoutManager);
        search_fragment_tabs_rv_price.setLayoutManager(priceLayoutManager);
        search_rv_ad.setOnRecyclerViewItemClickListener(this);

        searchFragment_sl.setOnRefreshListener(this);
        searchFragment_sl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        searchFragment_ll_tabs_location.setOnClickListener(this);
        searchFragment_ll_tabs_wishlist.setOnClickListener(this);
        searchFragment_ll_tabs_cusine.setOnClickListener(this);
        searchFragment_ll_tabs_price.setOnClickListener(this);
        searchFragment_ll_submit.setOnClickListener(this);

        search_fragment_iv_close_tab.setOnClickListener(this);

        final String userData = Utils.getString(getContext(), Constant.KEY_USER_DATA);
        if (!TextUtils.isEmpty(userData) && !userData.equalsIgnoreCase("null")) {
            userResponseModel = new Gson().fromJson(userData, UserResponseModel.class);
            Utils.storeString(getContext(), Constant.User_id, userResponseModel.getUser_id());
            final double lat = FlunkyApplication.getInstance().getLatitude();
            final double lang = FlunkyApplication.getInstance().getLongitude();
            if (lat == 0 && lang == 0) {
                showLocationEnableDialog();
            } else {
                getRestaurantDetail(userResponseModel.getUser_id(), Constant.APP_KEY, cusine.toString(), location.toString(), price.toString());
            }
        }
        getRestaurantSortMenuItems(Constant.APP_KEY);
    }
    private void getRestaurantSortMenuItems(String flunkyapp) {
        Call<ResponseOfAllApiModel> getRestaurantSOrtMenuItem = new RestClient().getApiInterface().getRestaurantSortMenu(flunkyapp, Constant.OS);
        getRestaurantSOrtMenuItem.enqueue(new RetrofitCallback<ResponseOfAllApiModel>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApiModel data) {
                setSortMenuData(data);
            }
        });
    }

    private void getRestaurantDetail(String userId, String appKey, String cusine, String location, String price) {
        final double lat = FlunkyApplication.getInstance().getLatitude();
        final double lang = FlunkyApplication.getInstance().getLongitude();
        Log.e("Lat", String.valueOf(lat));
        Call<ResponseOfAllApiModel> restoCall = new RestClient().getApiInterface().getRestaurantData(appKey, userId, String.valueOf(lat), String.valueOf(lang), "", cusine, location, price, wishlist, "1", Constant.OS);
        restoCall.enqueue(new RetrofitCallback<ResponseOfAllApiModel>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApiModel data) {
                if (data.getRestaurantResponseModel().isEmpty()) {
                    Logger.toast(getContext(), "No Data Available");
                } else {
                    restaurantResponseModels.clear();
                    restaurantResponseModels.trimToSize();
                    restaurantResponseModels.addAll(data.getRestaurantResponseModel());
                    search_rv_ad.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ResponseOfAllApiModel> call, Throwable error) {
                super.onFailure(call, error);
                wishlist = "0";
                removeTabsImageTextColor();
            }
        });
    }

    private void ChangeWishListItem(String rest_id, String isWishlist) {
        Call<JsonObject> removeItemFromWishlis = new RestClient().getApiInterface().removeItemFromWishlist(Constant.APP_KEY, Utils.getString(getContext(), Constant.User_id), isWishlist, rest_id, Constant.OS);
        removeItemFromWishlis.enqueue(new RetrofitCallback<JsonObject>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(JsonObject data) {
                Logger.toast(getContext(), data.get(Constant.KEY_MESSAGE).toString());
            }
            @Override
            public void onFailure(Call<JsonObject> call, Throwable error) {
                super.onFailure(call, error);
                Logger.toast(getContext(), error.getMessage().toString());
            }
        });
    }

    @Override
    protected void initToolbar() {
        MainActivity.getInstance().titleTextViewVisibility().setVisibility(View.GONE);
        MainActivity.getInstance().getRow_tool_bar_ll_menu().setVisibility(View.VISIBLE);
//        MainActivity.getInstance().getRow_tool_bar_ll_menu().setBackgroundColor(getResources().getColor(R.color.black));
        if (MainActivity.getInstance().secondTabTitle().getText().toString().equalsIgnoreCase("CHECK OUT")) {
            MainActivity.getInstance().secondTabTitle().setText(R.string.search);
            MainActivity.getInstance().secondTabSubTitle().setText(R.string.flunky_feed);
            MainActivity.getInstance().walletClick().setOnClickListener((View.OnClickListener) getActivity());
            MainActivity.getInstance().calenadrClick().setOnClickListener((View.OnClickListener) getActivity());
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_search_iv_close_tab:
                removeTabsImageTextColor();
                clearSelectedTabitemList();
                closeTabView();
                break;
            case R.id.fragment_search_ll_location:
                clearWishListTab();
                tabType = TabType.LOCATION;
                if (!restaurantSortMenuLocationModel.isEmpty()) {
                    search_fragment_restaurantSortMenu_ad = new RestaurantSortMenuItemsAdapter(getContext(), restaurantSortMenuLocationModel);
                    search_fragment_tabs_rv_location.setAdapter(search_fragment_restaurantSortMenu_ad);
                    search_fragment_restaurantSortMenu_ad.setOnCheckItemListener(this);
                    search_fragment_restaurantSortMenu_ad.notifyDataSetChanged();
                    search_fragment_ll_search_location_view.setVisibility(View.VISIBLE);
                    searchFragment_ll_search_cusine_view.setVisibility(View.GONE);
                    searchFragment_ll_search_price_view.setVisibility(View.GONE);
                    searchFragment_ll_submit.setVisibility(View.VISIBLE);
                } else {
                    Logger.toast(getContext(), "No data available");
                }
                break;
            case R.id.fragment_search_ll_cusine:
                clearWishListTab();
                tabType = TabType.CUSINE;
                if (!restaurantSortMenuCusineModel.isEmpty()) {
                    search_fragment_restaurantSortMenu_ad = new RestaurantSortMenuItemsAdapter(getContext(), restaurantSortMenuCusineModel);
                    search_fragment_restaurantSortMenu_ad.setOnCheckItemListener(this);
                    search_fragment_tabs_rv_cusine.setAdapter(search_fragment_restaurantSortMenu_ad);
                    search_fragment_restaurantSortMenu_ad.notifyDataSetChanged();
                    searchFragment_ll_search_cusine_view.setVisibility(View.VISIBLE);
                    search_fragment_ll_search_location_view.setVisibility(View.GONE);
                    searchFragment_ll_search_price_view.setVisibility(View.GONE);
                    searchFragment_ll_submit.setVisibility(View.VISIBLE);
                } else {
                    Logger.toast(getContext(), "No data available");
                }
                break;
            case R.id.fragment_search_ll_price:
                clearWishListTab();
                tabType = TabType.PRICE;
                if (!restaurantSortMenuPriceModel.isEmpty()) {
                    search_fragment_restaurantSortMenu_ad = new RestaurantSortMenuItemsAdapter(getContext(), restaurantSortMenuPriceModel);
                    search_fragment_restaurantSortMenu_ad.setOnCheckItemListener(this);
                    search_fragment_tabs_rv_price.setAdapter(search_fragment_restaurantSortMenu_ad);
                    search_fragment_restaurantSortMenu_ad.notifyDataSetChanged();
                    searchFragment_ll_search_price_view.setVisibility(View.VISIBLE);
                    search_fragment_ll_search_location_view.setVisibility(View.GONE);
                    searchFragment_ll_search_cusine_view.setVisibility(View.GONE);
                    searchFragment_ll_submit.setVisibility(View.VISIBLE);
                } else {
                    Logger.toast(getContext(), "No data available");
                }
                break;
            case R.id.fragment_search_ll_wishlist:
                tabType = TabType.WISHLIST;
                setOnWishlistTabSelect();
                break;
            case R.id.fragment_search_ll_submit:
                searchFragment_ll_submit.setVisibility(View.GONE);
                searchFragment_ll_search_price_view.setVisibility(View.GONE);
                search_fragment_ll_search_location_view.setVisibility(View.GONE);
                searchFragment_ll_search_cusine_view.setVisibility(View.GONE);
                setFeedMenuData();
                break;
        }
        FlunkyApplication.getInstance().setTabType(tabType);
    }
//if wishlist is selected than make it white //
    private void clearWishListTab() {
        wishlist = "0";
        search_fragment_tv_search_wishlist.setTextColor(getResources().getColor(R.color.white));
        search_fragment_iv_search_wishlist.setImageDrawable(getResources().getDrawable(R.drawable.ic_wishlist_white));
    }

    private void closeTabView() {
        switch (FlunkyApplication.getInstance().getTabType()) {
            case LOCATION:
                search_fragment_ll_search_location_view.setVisibility(View.GONE);
                search_fragment_tv_search_location.setTextColor(getResources().getColor(R.color.white));
                break;
            case PRICE:
                search_fragment_tv_search_price.setTextColor(getResources().getColor(R.color.white));
                searchFragment_ll_search_price_view.setVisibility(View.GONE);
                break;
            case CUSINE:
                search_fragment_tv_search_cusine.setTextColor(getResources().getColor(R.color.white));
                searchFragment_ll_search_cusine_view.setVisibility(View.GONE);
                break;
        }
        searchFragment_ll_submit.setVisibility(View.GONE);
    }

    private void setOnWishlistTabSelect() {
       clearAllTabs();
        clearSelectedTabitemList();
        if (wishlist.equalsIgnoreCase("0")) {
            wishlist = "1";
            search_fragment_tv_search_wishlist.setTextColor(getResources().getColor(R.color.light_green));
            search_fragment_iv_search_wishlist.setImageDrawable(getResources().getDrawable(R.drawable.ic_wishlist_green));
        } else {
            wishlist = "0";
            search_fragment_tv_search_wishlist.setTextColor(getResources().getColor(R.color.white));
            search_fragment_iv_search_wishlist.setImageDrawable(getResources().getDrawable(R.drawable.ic_wishlist_white));
        }

        searchFragment_ll_submit.setVisibility(View.GONE);
        searchFragment_ll_search_price_view.setVisibility(View.GONE);
        search_fragment_ll_search_location_view.setVisibility(View.GONE);
        searchFragment_ll_search_cusine_view.setVisibility(View.GONE);

        getRestaurantDetail(userResponseModel.getUser_id(), Constant.APP_KEY, cusine.toString(), location.toString(), price.toString());
    }

    private void clearSelectedTabitemList() {
        if(FlunkyApplication.getInstance().getTabType().equals(TabType.WISHLIST)){
         clearLocationList();
            clearCusinList();
            clearPriceList();
        }else{
          switch (FlunkyApplication.getInstance().getTabType()){
              case LOCATION:
                  clearLocationList();
                  break;
              case CUSINE:
                  clearCusinList();
                  break;
              case PRICE:
                  clearPriceList();
                  break;
          }
        }

    }
    private void clearLocationList(){
        for (int i = 0; i < locationAddList.size(); i++) {
            final RestaurantSortMenuItemsModel data = locationAddList.get(i);
            data.setIs_checked(false);
        }
        location.setLength(0);
        locationAddList.clear();
    }
    private void clearCusinList(){
        for (int i = 0; i < cusineAddList.size(); i++) {
            final RestaurantSortMenuItemsModel data = cusineAddList.get(i);
            data.setIs_checked(false);
        }
        cusine.setLength(0);
        cusineAddList.clear();
    }
    private void clearPriceList(){
        for (int i = 0; i < priceAddList.size(); i++) {
            final RestaurantSortMenuItemsModel data = priceAddList.get(i);
            data.setIs_checked(false);
        }

        price.setLength(0);
        priceAddList.clear();
    }

    private void setFeedMenuData() {
        location.setLength(0);
        cusine.setLength(0);
        price.setLength(0);
        if (!locationAddList.isEmpty()) {
            for (int i = 0; i < locationAddList.size(); i++) {
                location.append(locationAddList.get(i).getId());
                location.append(",");
            }
            location.setLength(location.length() - 1);
        }
        if (!cusineAddList.isEmpty()) {
            for (int i = 0; i < cusineAddList.size(); i++) {
                cusine.append(cusineAddList.get(i).getId());
                cusine.append(",");
            }
            cusine.setLength(cusine.length() - 1);
        }
        if (!priceAddList.isEmpty()) {
            for (int i = 0; i < priceAddList.size(); i++) {
                price.append(priceAddList.get(i).getId());
                price.append(",");
            }
            price.setLength(price.length() - 1);
        }
        getRestaurantDetail(userResponseModel.getUser_id(), Constant.APP_KEY, cusine.toString(), location.toString(), price.toString());
    }

    @Override
    public void onItemClick(int position, View view) {
        RestaurantResponseModel restaurantMenuResponseModel;
        final ArrayList<RestaurantMenuResponseModel> menu = restaurantResponseModels.get(position).getRestaurantMenuResponseModels();
        switch (view.getId()) {
            case R.id.row_fragment_search_iv_see_menu:
                final Bundle bundle = new Bundle();
                bundle.putParcelableArrayList("MenuList", menu);
                FragmentManager fm = getActivity().getFragmentManager();
                final SeeMenuDialog dialogFragment = new SeeMenuDialog();
                dialogFragment.setArguments(bundle);
                dialogFragment.show(fm, dialogFragment.getClass().getSimpleName());
                break;
            case R.id.row_fragment_seaarch_iv_more:
                restaurantMenuResponseModel = (RestaurantResponseModel) view.getTag();
                restaurantMenuResponseModel.setHas_more_clicked(true);
                restaurantResponseModels.remove(position);
                restaurantResponseModels.add(position, restaurantMenuResponseModel);
                search_rv_ad.notifyDataSetChanged();
                break;
            case R.id.row_fragment_seaarch_iv_more_close:
                restaurantMenuResponseModel = (RestaurantResponseModel) view.getTag();
                restaurantMenuResponseModel.setHas_more_clicked(false);
                restaurantResponseModels.remove(position);
                restaurantResponseModels.add(position, restaurantMenuResponseModel);
                search_rv_ad.notifyDataSetChanged();
                break;
            case R.id.row_fragment_search_iv_wishlist_white:
                final String add = "add";
                restaurantMenuResponseModel = (RestaurantResponseModel) view.getTag();
                restaurantMenuResponseModel.setIs_wishlist("1");
                restaurantResponseModels.remove(position);
                restaurantResponseModels.add(position, restaurantMenuResponseModel);
                search_rv_ad.notifyDataSetChanged();
                ChangeWishListItem(restaurantResponseModels.get(position).getRest_id(), add);
                break;
            case R.id.row_fragment_search_iv_wishlist_red:
                final String remove = "remove";
                restaurantMenuResponseModel = (RestaurantResponseModel) view.getTag();
                restaurantMenuResponseModel.setIs_wishlist("0");
                restaurantResponseModels.remove(position);
                restaurantResponseModels.add(position, restaurantMenuResponseModel);
                search_rv_ad.notifyDataSetChanged();
                ChangeWishListItem(restaurantResponseModels.get(position).getRest_id(), remove);
                break;
            case R.id.row_fragment_search_ll_gift_me:
                Utils.storeString(getContext(), Constant.RESTAURANT_ACC_ID, restaurantResponseModels.get(position).getUser_account_id());
                Bundle b = new Bundle();
                b.putString("rest_id", restaurantResponseModels.get(position).getRest_id());
                CheckOutFragment addEventSummaryFragment = new CheckOutFragment();
                addEventSummaryFragment.setArguments(b);
                addFragment(SearchFragment.this, addEventSummaryFragment, true);
                break;
        }
    }

    private void setSortMenuData(ResponseOfAllApiModel data) {
        if (data.getRestaurantSortMenuResponseModel() != null) {
            restaurantSortMenuLocationModel = data.getRestaurantSortMenuResponseModel().getRestaurantSortMenulocationData();
            restaurantSortMenuCusineModel = data.getRestaurantSortMenuResponseModel().getRestaurantSortMenuCusineData();
            restaurantSortMenuPriceModel = data.getRestaurantSortMenuResponseModel().getRestaurantSortMenuCostData();
            Utils.storeArrayList(getActivity(), Constant.EVENTS_LIST, data.getRestaurantSortMenuResponseModel().getRestaurantSortMenuEvents());
            Utils.storeArrayList(getActivity(), Constant.COUPAN_PRICE, data.getRestaurantSortMenuResponseModel().getRestaurantSortMenuCoupanPrice());
        }
    }


    @Override
    public void onItemCheck(int position, boolean is_checked, View view) {
        switch (FlunkyApplication.getInstance().getTabType()) {
            case LOCATION:
                final RestaurantSortMenuItemsModel sortMenuLocationItemsModel = restaurantSortMenuLocationModel.get(position);
                if (sortMenuLocationItemsModel.is_checked()) {
                    sortMenuLocationItemsModel.setIs_checked(false);
                    locationAddList.remove(sortMenuLocationItemsModel);
                } else {
                    sortMenuLocationItemsModel.setIs_checked(true);
                    locationAddList.add(sortMenuLocationItemsModel);
                }
                if (locationAddList.isEmpty()) {
                    removeTabsImageTextColor();
                } else {
                    changeTabsImageTextColor();
                }
                search_fragment_restaurantSortMenu_ad.notifyDataSetChanged();
                break;
            case PRICE:
                final RestaurantSortMenuItemsModel sortMenuPriceItemsModel = restaurantSortMenuPriceModel.get(position);
                if (sortMenuPriceItemsModel.is_checked()) {
                    sortMenuPriceItemsModel.setIs_checked(false);
                    priceAddList.remove(sortMenuPriceItemsModel);
                } else {
                    sortMenuPriceItemsModel.setIs_checked(true);
                    priceAddList.add(sortMenuPriceItemsModel);
                }
                if (priceAddList.isEmpty()) {
                    removeTabsImageTextColor();
                } else {
                    changeTabsImageTextColor();
                }
                search_fragment_restaurantSortMenu_ad.notifyDataSetChanged();
                break;
            case CUSINE:
                final RestaurantSortMenuItemsModel sortMenuCusineItemsModel = restaurantSortMenuCusineModel.get(position);
                if (sortMenuCusineItemsModel.is_checked()) {
                    sortMenuCusineItemsModel.setIs_checked(false);
                    cusineAddList.remove(sortMenuCusineItemsModel);
                } else {
                    sortMenuCusineItemsModel.setIs_checked(true);
                    cusineAddList.add(sortMenuCusineItemsModel);
                }
                if (cusineAddList.isEmpty()) {
                    removeTabsImageTextColor();
                } else {
                    changeTabsImageTextColor();
                }
                search_fragment_restaurantSortMenu_ad.notifyDataSetChanged();
                break;

        }
    }

    public void showLocationEnableDialog() {
        final LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(FlunkyApplication.getInstance().getLocationRequest());
        builder.setAlwaysShow(true);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(FlunkyApplication.getInstance().getGoogleApiClient(), builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        FlunkyApplication.getInstance().onConnected(null);
                        getRestaurantDetail(userResponseModel.getUser_id(), Constant.APP_KEY, cusine.toString(), location.toString(), price.toString());
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(getActivity(), Constant.REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    public void changeTabsImageTextColor() {
        switch (FlunkyApplication.getInstance().getTabType()) {
            case LOCATION:
                search_fragment_tv_search_location.setTextColor(getResources().getColor(R.color.light_green));
                search_fragment_iv_search_location.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_green));
                break;
            case PRICE:
                search_fragment_tv_search_price.setTextColor(getResources().getColor(R.color.light_green));
                search_fragment_iv_search_price.setImageDrawable(getResources().getDrawable(R.drawable.ic_price_green));
                break;
            case CUSINE:
                search_fragment_tv_search_cusine.setTextColor(getResources().getColor(R.color.light_green));
                search_fragment_iv_search_cusine.setImageDrawable(getResources().getDrawable(R.drawable.ic_cusine_green));
                break;

        }
    }

    public void removeTabsImageTextColor() {
        if(FlunkyApplication.getInstance().getTabType()!=null){
            switch (FlunkyApplication.getInstance().getTabType()) {
                case LOCATION:
                    search_fragment_tv_search_location.setTextColor(getResources().getColor(R.color.white));
                    search_fragment_iv_search_location.setImageDrawable(getResources().getDrawable(R.drawable.ic_location));
                    break;
                case PRICE:
                    search_fragment_tv_search_price.setTextColor(getResources().getColor(R.color.white));
                    search_fragment_iv_search_price.setImageDrawable(getResources().getDrawable(R.drawable.ic_price));
                    break;
                case CUSINE:
                    search_fragment_tv_search_cusine.setTextColor(getResources().getColor(R.color.white));
                    search_fragment_iv_search_cusine.setImageDrawable(getResources().getDrawable(R.drawable.ic_cusine));
                    break;
                case WISHLIST:
                    search_fragment_tv_search_wishlist.setTextColor(getResources().getColor(R.color.white));
                    search_fragment_iv_search_wishlist.setImageDrawable(getResources().getDrawable(R.drawable.ic_wishlist_white));
                    break;

            }
        }
    }
    public void clearAllTabs(){
        search_fragment_tv_search_location.setTextColor(getResources().getColor(R.color.white));
        search_fragment_iv_search_location.setImageDrawable(getResources().getDrawable(R.drawable.ic_location));
        search_fragment_tv_search_price.setTextColor(getResources().getColor(R.color.white));
        search_fragment_iv_search_price.setImageDrawable(getResources().getDrawable(R.drawable.ic_price));
        search_fragment_tv_search_cusine.setTextColor(getResources().getColor(R.color.white));
        search_fragment_iv_search_cusine.setImageDrawable(getResources().getDrawable(R.drawable.ic_cusine));
        search_fragment_tv_search_wishlist.setTextColor(getResources().getColor(R.color.white));
        search_fragment_iv_search_wishlist.setImageDrawable(getResources().getDrawable(R.drawable.ic_wishlist_white));
    }
}
