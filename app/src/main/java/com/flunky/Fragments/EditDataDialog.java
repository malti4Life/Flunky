package com.flunky.Fragments;

import android.app.Activity;
import android.app.Dialog;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.flunky.Adapters.EditSelectedDataAdapter;
import com.flunky.FlunkyApplication;
import com.flunky.Interfaces.OnRecyclerViewItemClickListener;
import com.flunky.Model.RestaurantSortMenuItemsModel;
import com.flunky.R;

import java.util.ArrayList;


public class EditDataDialog extends DialogFragment implements OnRecyclerViewItemClickListener {
    private TextView edit_data_dialog_tv;
    private RecyclerView edit_data_dialog_rv;
    private EditSelectedDataAdapter editSelectedDataAdapter;
    private ArrayList<RestaurantSortMenuItemsModel> occassionCoupanList;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_FRAME,R.style.AppTheme_Dialog_Theme);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_edit_data,container,false);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationStyle;
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }
    @Override
    public void onResume() {
        final WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.width = WindowManager.LayoutParams.MATCH_PARENT;
        p.height = WindowManager.LayoutParams.WRAP_CONTENT;
        p.gravity = Gravity.BOTTOM;
        getDialog().getWindow().setAttributes(p);
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }
    public void initView(View view) {
        if(getArguments()!=null){
            occassionCoupanList = getArguments().getParcelableArrayList("dataList");
        }
        edit_data_dialog_tv=(TextView)view.findViewById(R.id.edit_data_tv_title);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        edit_data_dialog_rv=(RecyclerView)view.findViewById(R.id.edit_data_rv);
        edit_data_dialog_rv.setLayoutManager(linearLayoutManager);
        editSelectedDataAdapter = new EditSelectedDataAdapter(getActivity(),occassionCoupanList);
        editSelectedDataAdapter.setOnRecyclerViewItemClickListener(this);
        edit_data_dialog_rv.setAdapter(editSelectedDataAdapter);
        setTitle();

    }

    private void setTitle() {
        switch (FlunkyApplication.getInstance().getTabType()){
            case COUPANPRICE:
                edit_data_dialog_tv.setText(R.string.select_coupan_value);
                break;
            case OCCASSION:
                edit_data_dialog_tv.setText(getResources().getString(R.string.select_the_occasion));
                break;
        }
    }

    @Override
    public void onItemClick(int position, View view) {
        Intent i = new Intent();
        final String data = occassionCoupanList.get(position).getValue();
        final String id = occassionCoupanList.get(position).getId();
        i.putExtra("Data",data);
        i.putExtra("id",id);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, i);
        dismiss();
    }

}
