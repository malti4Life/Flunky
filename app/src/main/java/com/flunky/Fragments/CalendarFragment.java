package com.flunky.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.Nullable;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;


import com.flunky.Activities.MainActivity;
import com.flunky.Adapters.EventDetailAdapter;
import com.flunky.Interfaces.OnEventDataGetListener;
import com.flunky.Interfaces.OnRecyclerViewItemClickListener;
import com.flunky.Model.EventDataModel;
import com.flunky.Model.ResponseOfAllApiModel;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.TabType;
import com.flunky.utils.Utils;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;
import com.google.gson.JsonObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;


public class CalendarFragment extends BaseFragment implements View.OnClickListener, OnEventDataGetListener, OnRecyclerViewItemClickListener {
    private RecyclerView fragment_calendar_rv;
    private EventDetailAdapter eventDetailAdapter;
    private TextView fagement_calendar_tv_addEvent;
    private ArrayList<EventDataModel>eventDataModelArrayList;
    private ArrayList<String> eventsList;
    private String actualMonth;
    private String year;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_calendar, container, false);
    }

    @Override
    protected void initView(View view) {

        eventDataModelArrayList=new ArrayList<EventDataModel>();
        eventsList= new ArrayList<>();
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        fragment_calendar_rv=(RecyclerView)view.findViewById(R.id.fragment_calendar_rv);
        eventDetailAdapter = new EventDetailAdapter(getContext(),eventDataModelArrayList);
        eventDetailAdapter.setOnRecyclerViewItemClickListener(this);
        fagement_calendar_tv_addEvent = (TextView) view.findViewById(R.id.fragment_calendar_tv_addEvent);
        CustomCalendarViewFragment caldroidFragment = new CustomCalendarViewFragment();
        caldroidFragment.setOnArrowClickListener(this);
        FragmentManager fragManager = getActivity().getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction t = fragManager.beginTransaction();
        t.add(R.id.child_fragment_container, caldroidFragment);
        t.commit();
        fragment_calendar_rv.setLayoutManager(linearLayoutManager);
        fragment_calendar_rv.setAdapter(eventDetailAdapter);
        fagement_calendar_tv_addEvent.setOnClickListener(this);

    }

    @Override
    protected void initToolbar() {
        MainActivity.getInstance().getRow_tool_bar_ll_menu().setBackgroundColor(getResources().getColor(R.color.black));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.fragment_calendar_tv_addEvent:
                addFragment(CalendarFragment.this, new AddEventFragment(), true);
                break;
        }
    }

    @Override
    public void onArrowClick(String month, String year) {
        actualMonth = String.valueOf(Integer.valueOf(month)+1);
        this.year=year;
        getEvent(actualMonth,year);

    }

    private void getEvent(String actualMonth, String year) {
        final ProgressDialog progressDialog = Logger.showProgressDialog(getContext());
        Call<ResponseOfAllApiModel>getEvents = new RestClient().getApiInterface().getEventDetail(Constant.APP_KEY, Utils.getString(getContext(),Constant.User_id),actualMonth,year);
        getEvents.enqueue(new RetrofitCallback<ResponseOfAllApiModel>(getContext(),progressDialog) {
            @Override
            public void onSuccess(ResponseOfAllApiModel data) {
                if(data.getEventDataModelArrayList()!=null){
                    eventDataModelArrayList.clear();
                    eventDataModelArrayList.trimToSize();
                    eventDataModelArrayList.addAll(data.getEventDataModelArrayList());
                    eventDetailAdapter.notifyDataSetChanged();
                    try {
                        setEvents(eventDataModelArrayList);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
            @Override
            public void onFailure(Call<ResponseOfAllApiModel> call, Throwable error) {
              progressDialog.dismiss();
                eventDataModelArrayList.clear();
                eventDetailAdapter.notifyDataSetChanged();
                try {
                    setEvents(eventDataModelArrayList);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private void setEvents(ArrayList<EventDataModel> eventDataModelArrayList) throws ParseException {
        eventsList.clear();
        if(!eventDataModelArrayList.isEmpty()){
            for(int i = 0; i<eventDataModelArrayList.size(); i++){
                final String finalString = eventDataModelArrayList.get(i).getEvent_date();
                final Date initDate = new SimpleDateFormat(Constant.EXPECTED_DATE_FORMATTER).parse(finalString);
                final SimpleDateFormat formatter = new SimpleDateFormat(Constant.API_DATE_TIME_FORMATTER);
                final String parsedDate = formatter.format(initDate);
                eventsList.add(parsedDate);
                Log.e("converted Date",parsedDate);
            }
        }

        CustomCalendarViewFragment.getInstance().calendarAdapter.setItems(eventsList);
        CustomCalendarViewFragment.getInstance().calendarAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(int position, View view) {
        switch (view.getId()){
            case R.id.row_event_tv_giftMe:
                MainActivity.getInstance().setBackground(TabType.SEARCH);
                break;
            case R.id.row_event_iv_delete_event:
                showEventRemoveDialog(position);
                break;
        }

    }
    private void showEventRemoveDialog(final int position) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext(),R.style.AppTheme_AlertDialog);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("Are you sure to remove this event");
        builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeEvent(eventDataModelArrayList.get(position).getCal_id());
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.create().show();
    }
    private void removeEvent(String cal_id) {
        Call<JsonObject>removeEvent=new RestClient().getApiInterface().removeEvent(Constant.APP_KEY,cal_id);
        removeEvent.enqueue(new RetrofitCallback<JsonObject>(getContext(),Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(JsonObject data) {
                if(data.get("res_code").getAsString().equalsIgnoreCase("1")){
                   Logger.toast(getContext(),data.get("res_message").toString());
                    getEvent(actualMonth,year);
                }
            }
        });
    }
}