package com.flunky.Fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import com.flunky.Activities.MainActivity;
import com.flunky.Model.UserResponseModel;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.exception.AuthenticationException;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;
import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;

public class CheckOutSummaryFragment extends BaseFragment implements View.OnClickListener {
    private FrameLayout fragment_checkout_summary_sv;
    private LinearLayout fragment_checkout_summary_ll_summary;
    private LinearLayout fragment_checkoput_summary_ll_creditCardView;
    private CardInputWidget fragment_checkout_summary_cw_creditCard;
    private LinearLayout fragment_checkout_summary_ll_congo;
    private LinearLayout fragment_checkout_summary_ll_image_view;

    private TextView fragment_checkout_summary_tv_from;
    private TextView fragment_checkout_summary_tv_receiver_name;
    private TextView fragment_checkout_summary_tv_occasion;
    private TextView fragment_checkout_summary_tv_value;
    private TextView fragment_checkout_summary_tv_send_time;
    private TextView fragment_checkout_summary_tv_message;
    private TextView fragment_checkout_summary_tv_purchase;
    private TextView fragment_checkout_summary_tv_home;

    private ImageView fragment_checkout_summary_iv_closeBuyNow;

    private TextView fragment_checkout_summary_tv_cardPay;

    private ImageView fragment_checkout_summary_iv_image;
    private String imagePath;
    private String receiverName;
    private String contactNo;
    private String occasionId;
    private String occasion;
    private String is_Now;
    private String message;
    private String valueId;
    private String value;
    private String senddate;
    private String rest_id;
    private String occasionDate;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_checkout_summary,container,false);
    }
    @Override
    protected void initView(View view) {


        if(getArguments()!=null){
            imagePath=getArguments().getString("imagePath");
            receiverName=getArguments().getString("receiverName");
            contactNo=getArguments().getString("receiverPhone");
            occasionId=getArguments().getString("occasionId");
            occasion=getArguments().getString("occasion");
            is_Now=getArguments().getString("isNow");
            message=getArguments().getString("personalMessage");
            valueId=getArguments().getString("value_id");
            value=getArguments().getString("value");
            senddate=getArguments().getString("sendDate");
            rest_id=getArguments().getString("rest_id");
            occasionDate = getArguments().getString("occasionDate");
        }
        fragment_checkout_summary_sv=(FrameLayout)view.findViewById(R.id.fragment_checkout_summary_sv);
        fragment_checkout_summary_ll_summary=(LinearLayout)view.findViewById(R.id.fragment_checkout_summary_ll_summary);
        fragment_checkoput_summary_ll_creditCardView=(LinearLayout)view.findViewById(R.id.fragment_checkout_summary_ll_creditCardView);
        fragment_checkout_summary_cw_creditCard=(CardInputWidget)view.findViewById(R.id.fragment_checkout_summary_cw_creditCard);
        fragment_checkout_summary_ll_congo=(LinearLayout)view.findViewById(R.id.fragment_checkout_summary_ll_congo);
        fragment_checkout_summary_ll_image_view=(LinearLayout)view.findViewById(R.id.fragment_checkout_summary_ll_image_view);

        fragment_checkout_summary_tv_from=(TextView)view.findViewById(R.id.fragment_checkout_summary_tv_sender_name);
        fragment_checkout_summary_tv_receiver_name=(TextView)view.findViewById(R.id.fragment_checkout_summary_tv_contactName);
        fragment_checkout_summary_tv_occasion=(TextView)view.findViewById(R.id.fragment_checkout_summary_tv_occasion);
        fragment_checkout_summary_tv_value=(TextView)view.findViewById(R.id.fragment_checkout_summary_tv_occasion_value);
        fragment_checkout_summary_tv_send_time=(TextView)view.findViewById(R.id.fragment_checkout_summary_tv_send_time);
        fragment_checkout_summary_tv_message=(TextView)view.findViewById(R.id.fragment_checkout_summary_tv_message);
        fragment_checkout_summary_iv_image=(ImageView) view.findViewById(R.id.fragment_checkout_summary_iv_first);
        fragment_checkout_summary_tv_purchase=(TextView)view.findViewById(R.id.fragment_checkout_summary_tv_purchase);
        fragment_checkout_summary_tv_home=(TextView)view.findViewById(R.id.fragment_checkout_summary_tv_home);
        fragment_checkout_summary_iv_closeBuyNow=(ImageView)view.findViewById(R.id.fragment_checkout_summary_iv_cloeseBuyNow);

        fragment_checkout_summary_tv_cardPay=(TextView)view.findViewById(R.id.fragment_checkout_summary_tv_cardPay);
        fragment_checkout_summary_tv_cardPay.setText(getString(R.string.pay)+value);
        setData();

        fragment_checkout_summary_iv_closeBuyNow.setOnClickListener(this);
        fragment_checkout_summary_tv_purchase.setOnClickListener(this);
        fragment_checkout_summary_tv_home.setOnClickListener(this);
        fragment_checkout_summary_tv_cardPay.setOnClickListener(this);
    }

    private void setData() {
        final String userData = Utils.getString(getContext(), Constant.KEY_USER_DATA);
        if (!TextUtils.isEmpty(userData) && !userData.equalsIgnoreCase("null")) {
            final UserResponseModel userResponseModel = new Gson().fromJson(userData, UserResponseModel.class);
            fragment_checkout_summary_tv_from.setText(Utils.getUpperCaseString(userResponseModel.getFirst_name()));
        }
        fragment_checkout_summary_tv_receiver_name.setText(receiverName);
        fragment_checkout_summary_tv_occasion.setText(occasion);
        fragment_checkout_summary_tv_value.setText(value);
        if(!TextUtils.isEmpty(message)|| message.equalsIgnoreCase("")){
            fragment_checkout_summary_tv_message.setText("No message added");

        }else{
            fragment_checkout_summary_tv_message.setText(message);
        }

        if(is_Now.equalsIgnoreCase("1")){
            fragment_checkout_summary_tv_send_time.setText("Now");
        }else{
            fragment_checkout_summary_tv_send_time.setText(senddate);
        }
        if(!TextUtils.isEmpty(imagePath)){
            fragment_checkout_summary_ll_image_view.setVisibility(View.VISIBLE);
            Bitmap bitmap = BitmapFactory.decodeFile(imagePath);
            fragment_checkout_summary_iv_image.setImageBitmap(bitmap);
        }else{

        }
    }


    private void doPurchase(String stripeToken) {
        final File file;
        final MultipartBody.Part imageOne;
        final String mediaType = "text/plain";


        if(!TextUtils.isEmpty(imagePath)){
            file = new File(imagePath);
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), file);
            imageOne = MultipartBody.Part.createFormData("image_one", file.getName(), reqFile);
        }else{
            imageOne = MultipartBody.Part.createFormData("image_one", " ");
        }
        final RequestBody apiKey = RequestBody.create(MediaType.parse(mediaType), Constant.APP_KEY);
        final RequestBody user_id = RequestBody.create(MediaType.parse(mediaType), Utils.getString(getContext(),Constant.User_id));
        final RequestBody receiver_name = RequestBody.create(MediaType.parse(mediaType),receiverName);
        final RequestBody receiver_phone = RequestBody.create(MediaType.parse(mediaType), contactNo);
        final RequestBody occassioId = RequestBody.create(MediaType.parse(mediaType),occasionId);
        final RequestBody occassion_date = RequestBody.create(MediaType.parse(mediaType),occasionDate );
        final RequestBody send_date = RequestBody.create(MediaType.parse(mediaType),senddate);
        final RequestBody isNow = RequestBody.create(MediaType.parse(mediaType),is_Now);
        final RequestBody personalMessage =RequestBody.create(MediaType.parse(mediaType), message);
        final RequestBody amountId=RequestBody.create(MediaType.parse(mediaType),valueId);
        final RequestBody stripeAmount=RequestBody.create(MediaType.parse(mediaType),value.replace("$",""));
        final RequestBody stripeRestroAccid=RequestBody.create(MediaType.parse(mediaType),Utils.getString(getContext(),Constant.RESTAURANT_ACC_ID));
        final RequestBody stripetoken=RequestBody.create(MediaType.parse(mediaType),stripeToken);
        final RequestBody restaurantId=RequestBody.create(MediaType.parse(mediaType),rest_id);
        final RequestBody OsType = RequestBody.create(MediaType.parse(mediaType),Constant.OS);

        Call<JsonObject> responseModelCall = RestClient.getInstance().getApiInterface().
                getCheckout(imageOne,apiKey,user_id,receiver_name,receiver_phone,occassioId,occassion_date,send_date,isNow,personalMessage,amountId,stripeAmount,stripeRestroAccid,stripetoken,restaurantId,OsType);
        responseModelCall.enqueue(new RetrofitCallback<JsonObject>(getContext(), Logger.showProgressDialog(getActivity())) {
            @Override
            public void onSuccess(JsonObject response) {
                Log.e("response", response.toString());
                if(response.get("res_code").toString().equalsIgnoreCase("0")){
                    Logger.toast(getContext(),"Something went wrong .Try again!!");
                }else{
                    Utils.storeString(getContext(),Constant.RESTAURANT_ACC_ID,"");
                    fragment_checkout_summary_sv.setBackgroundColor(getResources().getColor(R.color.black));
                    MainActivity.getInstance().getRow_tool_bar_ll_menu().setBackgroundColor(getResources().getColor(R.color.black));
                    fragment_checkout_summary_ll_summary.setVisibility(View.GONE);
                    fragment_checkoput_summary_ll_creditCardView.setVisibility(View.GONE);
                    fragment_checkout_summary_ll_congo.setVisibility(View.VISIBLE);
                }

            }
        });
    }

    @Override
    protected void initToolbar() {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fragment_checkout_summary_tv_purchase:
                setCardView();
                break;
            case R.id.fragment_checkout_summary_iv_cloeseBuyNow:
                removeCardView();
                break;
            case R.id.fragment_checkout_summary_tv_home:
                if (getActivity().getSupportFragmentManager().getBackStackEntryCount() > 0) {
                    getActivity().getSupportFragmentManager().popBackStack();
                }
                displayFragment(new SearchFragment(),false);
                break;
            case R.id.fragment_checkout_summary_tv_cardPay:
                validateCardData();
                break;

        }
    }

    private void removeCardView() {
        MainActivity.getInstance().getRow_tool_bar_ll_menu().setBackgroundColor(getResources().getColor(R.color.black));
        fragment_checkoput_summary_ll_creditCardView.setVisibility(View.GONE);
        fragment_checkout_summary_ll_summary.setBackgroundColor(getResources().getColor(R.color.black));
        fragment_checkout_summary_sv.setBackgroundColor(getResources().getColor(R.color.black));
        fragment_checkout_summary_ll_summary.setAlpha(1);
        fragment_checkout_summary_ll_summary.setFocusable(true);
        fragment_checkout_summary_ll_summary.setFocusableInTouchMode(true);
        fragment_checkout_summary_tv_purchase.setOnClickListener(this);
    }

    private void setCardView() {
        MainActivity.getInstance().getRow_tool_bar_ll_menu().setBackgroundColor(getResources().getColor(R.color.thick_black));
        fragment_checkoput_summary_ll_creditCardView.setVisibility(View.VISIBLE);
        fragment_checkout_summary_ll_summary.setBackgroundColor(getResources().getColor(R.color.thick_black));
        fragment_checkout_summary_sv.setBackgroundColor(getResources().getColor(R.color.thick_black));
        fragment_checkout_summary_ll_summary.setAlpha(0.4f);
        fragment_checkout_summary_ll_summary.setFocusable(false);
        fragment_checkout_summary_ll_summary.setFocusableInTouchMode(false);
        fragment_checkout_summary_tv_purchase.setOnClickListener(null);
    }

    private void validateCardData() {
        Card cardToSave =  fragment_checkout_summary_cw_creditCard.getCard();
        if (cardToSave==null) {
            Logger.showSnackBar(getContext(),"Invalid Card Data");
        }else{
            setStripeCard(cardToSave);
        }
    }
    private void setStripeCard(Card card) {
        final ProgressDialog proggressDialog=Logger.showProgressDialog(getContext());

        Stripe stripe = null;
        try {
            stripe = new Stripe(getContext(),"pk_test_2vnD5XrSl4lLqdcwGFu0qZ4O");
        } catch (AuthenticationException e) {
            e.printStackTrace();
        }
        stripe.createToken(
                card,
                new TokenCallback() {
                    public void onSuccess(Token token) {
                        Log.e("Token",token.getId().toString());
                        Logger.dismissProgressDialog(proggressDialog);
                        doPurchase(token.getId().toString());
                    }
                    public void onError(Exception error) {
                        error.getMessage();
                        Logger.showSnackBar(getContext(),  error.getMessage());
                        Logger.dismissProgressDialog(proggressDialog);
                    }
                }
        );
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
