package com.flunky.Fragments;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flunky.Adapters.SeeMenuAdapter;
import com.flunky.Model.RestaurantMenuResponseModel;
import com.flunky.R;

import java.util.ArrayList;

public class SeeMenuDialog extends DialogFragment implements View.OnClickListener {
    private TextView see_menu_iv_close;
    private RecyclerView see_menu_dialog_rv;
    private SeeMenuAdapter seeMenuAdapter;
    private ArrayList<RestaurantMenuResponseModel> restaurantMenuArrayList;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_see_menu, container, false);
    }


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    @Override
    public void onResume() {
        final WindowManager.LayoutParams p = getDialog().getWindow().getAttributes();
        p.width = WindowManager.LayoutParams.MATCH_PARENT;
        p.height = WindowManager.LayoutParams.MATCH_PARENT;
        p.gravity = Gravity.TOP;
        getDialog().getWindow().setAttributes(p);
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView(view);
    }

    public void initView(View view) {
        restaurantMenuArrayList=new ArrayList<>();
        if (getArguments() != null) {
            restaurantMenuArrayList = getArguments().getParcelableArrayList("MenuList");
            Log.e("menudata",restaurantMenuArrayList.get(0).getRestaurantRecommendationResponseModels().get(0).getTitle());

        }
        see_menu_iv_close=(TextView) view.findViewById(R.id.see_menu_iv_close);
        see_menu_dialog_rv=(RecyclerView)view.findViewById(R.id.layout_see_menu_rv);
        see_menu_dialog_rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        seeMenuAdapter = new SeeMenuAdapter(getActivity(),restaurantMenuArrayList);
        see_menu_dialog_rv.setAdapter(seeMenuAdapter);
        see_menu_iv_close.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.see_menu_iv_close:
                dismiss();
        }
    }
}
