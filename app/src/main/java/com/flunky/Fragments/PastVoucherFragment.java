package com.flunky.Fragments;


import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.flunky.Activities.MainActivity;
import com.flunky.Adapters.WalletRecyclerViewAdapter;

import com.flunky.Interfaces.OnRecyclerViewItemClickListener;
import com.flunky.Model.ResponseOfAllApiModel;
import com.flunky.Model.WalletDetailsModel;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;
import com.flunky.webservices.RestClient;
import com.flunky.webservices.RetrofitCallback;

import java.util.ArrayList;

import retrofit2.Call;

public class PastVoucherFragment extends BaseFragment implements RadioGroup.OnCheckedChangeListener, OnRecyclerViewItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout fragment_past_vocher_sl;

    private RadioGroup fragment_past_voucher_rg;
    private RadioButton fragment_past_voucher_rb_purchased;
    private RadioButton fragment_past_voucher_rb_received;
    private RecyclerView fragment_past_voucher_rv;
    private WalletRecyclerViewAdapter walletRecyclerViewAdapter;
    private ArrayList<WalletDetailsModel> voucherList;
    private String action;
    private Typeface boldfont;
    private Typeface regularfont;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_past_vouchers, container, false);
    }

    @Override
    protected void initView(View view) {
        boldfont = Typeface.createFromAsset(getActivity().getAssets(),"fonts/AvenirNextCondensed-BoldItalic.ttf");
        regularfont =Typeface.createFromAsset(getActivity().getAssets(),"fonts/courier.ttf");
        voucherList=new ArrayList<>();
        fragment_past_vocher_sl=(SwipeRefreshLayout)view.findViewById(R.id.fragment_past_voucher_sl);
        fragment_past_voucher_rv = (RecyclerView) view.findViewById(R.id.fragment_past_voucher_rv);
        fragment_past_voucher_rv.setLayoutManager(new LinearLayoutManager(getContext()));
        fragment_past_voucher_rg = (RadioGroup)view.findViewById(R.id.fragment_past_voucher_rg);
        fragment_past_voucher_rb_received=(RadioButton)view.findViewById(R.id.fragment_past_voucher_rb_received);
        fragment_past_voucher_rb_purchased=(RadioButton)view.findViewById(R.id.fragment_past_voucher_rb_past);
        walletRecyclerViewAdapter = new WalletRecyclerViewAdapter(getContext(),voucherList);
        walletRecyclerViewAdapter.setOnRecyclerViewItemClickListener(this);
        fragment_past_voucher_rv.setAdapter(walletRecyclerViewAdapter);

        fragment_past_voucher_rg.setOnCheckedChangeListener(this);
        fragment_past_vocher_sl.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        fragment_past_vocher_sl.setOnRefreshListener(this);
        action = "past";
        getVoucherDetail();
    }
    private void getVoucherDetail() {
        Call<ResponseOfAllApiModel> getWalletData=new RestClient().getApiInterface().getVoucherDetails(Constant.APP_KEY, Utils.getString(getContext(),Constant.User_id),action,Constant.OS);
        getWalletData.enqueue(new RetrofitCallback<ResponseOfAllApiModel>(getContext(), Logger.showProgressDialog(getContext())) {
            @Override
            public void onSuccess(ResponseOfAllApiModel data) {
                if(!data.getWalletDetailsModelArrayList().isEmpty()){
                    voucherList.clear();
                    voucherList.trimToSize();
                    voucherList.addAll(data.getWalletDetailsModelArrayList());
                    walletRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<ResponseOfAllApiModel> call, Throwable error) {
                super.onFailure(call, error);


            }
        });
    }
    @Override
    protected void initToolbar() {
        MainActivity.getInstance().titleTextViewVisibility().setVisibility(View.VISIBLE);
        MainActivity.getInstance().setActionBarTitle(getString(R.string.past_vouchers));
        MainActivity.getInstance().getRow_tool_bar_ll_menu().setVisibility(View.GONE);
    }


    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId){
            case R.id.fragment_past_voucher_rb_past:
                fragment_past_voucher_rb_purchased.setTypeface(boldfont);
                fragment_past_voucher_rb_received.setTypeface(regularfont);
                fragment_past_voucher_rb_received.setText("received");
                fragment_past_voucher_rb_purchased.setText("PURCHASED");
                action="past";
                voucherList.clear();
                walletRecyclerViewAdapter.notifyDataSetChanged();
                getVoucherDetail();
                break;
            case R.id.fragment_past_voucher_rb_received:
                fragment_past_voucher_rb_purchased.setTypeface(regularfont);
                fragment_past_voucher_rb_received.setTypeface(boldfont);
                fragment_past_voucher_rb_received.setText("RECEIVED");
                fragment_past_voucher_rb_purchased.setText("purchased");
                action="received";
                voucherList.clear();
                walletRecyclerViewAdapter.notifyDataSetChanged();
                getVoucherDetail();
                break;
        }
    }

    @Override
    public void onItemClick(int position, View view) {
        final WalletDetailsModel walletDetailsModel ;
        switch (view.getId()){
            case R.id.row_fragment_wallet_iv_more:
                walletDetailsModel=(WalletDetailsModel) view.getTag();
                walletDetailsModel.setHas_more_clicked(true);
                voucherList.remove(position);
                voucherList.add(position,walletDetailsModel);
                walletRecyclerViewAdapter.notifyDataSetChanged();
                break;
            case R.id.row_fragment_wallet_iv_more_close:
                walletDetailsModel=(WalletDetailsModel) view.getTag();
                walletDetailsModel.setHas_more_clicked(false);
                voucherList.remove(position);
                voucherList.add(position,walletDetailsModel);
                walletRecyclerViewAdapter.notifyDataSetChanged();
                break;


        }
    }

    @Override
    public void onRefresh() {
        voucherList.clear();
        getVoucherDetail();
        fragment_past_vocher_sl.setRefreshing(false);

    }
}
