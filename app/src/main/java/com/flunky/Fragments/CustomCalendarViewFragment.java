package com.flunky.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flunky.Adapters.CalendarAdapter;
import com.flunky.Interfaces.OnEventDataGetListener;
import com.flunky.Interfaces.OnRecyclerViewItemClickListener;
import com.flunky.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class CustomCalendarViewFragment extends BaseFragment{
    private static CustomCalendarViewFragment instance;
    private  TextView title;
    private GridView gridView;
    public Calendar month, itemmonth;
    public CalendarAdapter calendarAdapter;

    public ArrayList<String> items;
    private OnEventDataGetListener onEventDataGetListener;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_custom_calendar,container,false);
    }
    @Override
    protected void initView(View view) {
        instance = this;
        month = Calendar.getInstance();
        itemmonth =(Calendar)month.clone();
        items = new ArrayList<String>();
        calendarAdapter=new CalendarAdapter(getActivity(), (GregorianCalendar) month);
        gridView = (GridView)view.findViewById(R.id.gridview);
        gridView.setAdapter(calendarAdapter);
        title = (TextView)view.findViewById(R.id.month_name);
        title.setText(android.text.format.DateFormat.format("MMMM yyy",month));
        RelativeLayout previous = (RelativeLayout)view.findViewById(R.id.previous);
        previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPreviousMonth();
                refreshCalendar();
            }
        });
        RelativeLayout next = (RelativeLayout)view.findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setNextMonth();
                refreshCalendar();
            }
        });
        onEventDataGetListener.onArrowClick(String.valueOf(month.get(Calendar.MONTH)),String.valueOf(month.get(Calendar.YEAR)));
//        items.add("2016-12-05");
//        items.add("2016-12-27");
//        items.add("2016-12-15");
//        items.add("2016-11-20");
//        items.add("2016-11-30");
//        items.add("2016-11-28");
//        calendarAdapter.setItems(items);
//        calendarAdapter.notifyDataSetChanged();
    }
    @Override
    protected void initToolbar() {

    }
    public static CustomCalendarViewFragment getInstance()
    {
        return instance;
    }
    public CalendarAdapter calendarAdapter(){
        return calendarAdapter;
    }
    private void setNextMonth() {
        if (month.get(Calendar.MONTH) == month.getActualMaximum(Calendar.MONTH)) {
            month.set((month.get(Calendar.YEAR) + 1),
                    month.getActualMinimum(Calendar.MONTH), 1);
        } else {
            month.set(Calendar.MONTH, month.get(Calendar.MONTH) + 1);
        }
        onEventDataGetListener.onArrowClick(String.valueOf(month.get(Calendar.MONTH)),String.valueOf(month.get(Calendar.YEAR)));
    }

    private void setPreviousMonth() {
        if (month.get(Calendar.MONTH) == month.getActualMinimum(Calendar.MONTH)) {
            month.set((month.get(Calendar.YEAR) - 1),
                    month.getActualMaximum(Calendar.MONTH), 1);
        } else {
            month.set(Calendar.MONTH, month.get(Calendar.MONTH) - 1);
        }
        onEventDataGetListener.onArrowClick(String.valueOf(month.get(Calendar.MONTH)),String.valueOf(month.get(Calendar.YEAR)));

    }
    private void refreshCalendar() {
        calendarAdapter.refreshDays();
        calendarAdapter.notifyDataSetChanged();
        title.setText(android.text.format.DateFormat.format("MMMM yyyy", month));
    }


    public void setOnArrowClickListener(OnEventDataGetListener onEventDataGetListener){
        this.onEventDataGetListener=onEventDataGetListener;
    }
}
