/*
 * Copyright (c) 2015. $user
 */

package com.flunky.facebook;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.CallbackManager.Factory;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.share.model.ShareContent;
import com.facebook.share.widget.ShareDialog;
import com.flunky.R;
import com.flunky.utils.Logger;

import java.security.MessageDigest;
import java.util.Collection;

/**
 * <p/>
 *

 *         </p>
 *         Class is used when user want to login with facebook sdk login, get user profile, share Video/Image to wall post.
 *         we have used Facebook-sdk 4.0.1
 */
public class FacebookAPI {

    private static FacebookAPI instance;

    private CallbackManager callbackManager;

    private Method currentMethods;

    private FacebookInterface mGraphResponse;

    private Collection<String> permissions;

    private ShareContent mShareContent;


    public FacebookAPI() {
        instance = this;
    }

    /**
     * Get instance of FacebookAPI for accessing all public method.
     *
     * @return FacebookAPI current instance of FacebookAPI
     */
    public static FacebookAPI getInstance() {
        return instance;
    }

    /**
     * When Activity was started at that time for initialization of facebook we have to add this method to onCreate()
     * In this method we also registering callback of login which has 3 override method.
     * On successful login of user onSuccess() is called.
     * If User cancel dialog of login event at that time onCancel() is called.
     * If any error occurs at that time onError() is called.
     *
     * @param context Activity context
     */
    public final void init(final Activity context) {
        FacebookSdk.sdkInitialize(context);
        this.callbackManager = Factory.create();
        LoginManager.getInstance().registerCallback(this.callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Logger.e("onsuccess called");
                AccessToken.setCurrentAccessToken(loginResult.getAccessToken());
                switch (FacebookAPI.this.currentMethods) {
                    case PROFILE:
                        FacebookAPI.this.meRequest();
                        break;
                    case SHARE:
                        FacebookAPI.this.doPost(context);
                        break;
                }
            }

            @Override
            public void onCancel() {
                Logger.e("oncancel called");
                Logger.dialog(context, context.getString(R.string.app_name), "User has canceled login");
            }

            @Override
            public void onError(FacebookException e) {
                Logger.e("onerror called");
                Logger.dialog(context, context.getString(R.string.app_name), "Some error occurs while login to facebook.");
            }
        });
    }


    /**
     * For Login
     *
     * @param context Application/Activity context.
     */
    public final void logIn(Activity context) {
        if (this.getPermissions() != null) {
            LoginManager.getInstance().logInWithReadPermissions(context, this.getPermissions());
        } else {
            LoginManager.getInstance().logInWithReadPermissions(context, null);
        }

    }

    /**
     * Getting profile detail of login user.
     * If user is not logged in then first user will be login and then get his/her profile details.
     *
     * @param activity      Current activity
     * @param method        PROFILE method will be used here for getting user profile details.
     * @param graphResponse new instance of FacebookInterface interface.
     */
    public final void facebookMeRequest(final Activity activity, final Method method, final FacebookInterface graphResponse) {
        this.currentMethods = method;
        this.mGraphResponse = graphResponse;
        if (AccessToken.getCurrentAccessToken() != null) {
            this.meRequest();
        } else {
            this.logIn(activity);
        }
    }



    /**
     * this private method is for getting facebook login user detail.
     * using GraphRequest api.
     */
    private final void meRequest() {
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,first_name,last_name,email,age_range,relationship_status");
        GraphRequest request = new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/me",
                parameters,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {

                        if (response.getJSONObject() != null) {
                            Logger.e(response.getJSONObject().toString());
                            FacebookAPI.this.mGraphResponse.getMyProfile(response.getJSONObject());
                        }

                    }
                }
        );
        request.executeAsync();
    }

    /**
     * Clear access token from shared preference and logout from facebook.
     */
    public final void logout() {
        if (AccessToken.getCurrentAccessToken() != null && !AccessToken.getCurrentAccessToken().isExpired()) {
            AccessToken.setCurrentAccessToken(null);
            LoginManager.getInstance().logOut();
        }

    }

    /**
     * Get instance of CallbackManager.
     *
     * @return callbackManager which is initialize in init().
     */
    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    /**
     * Get all permission which is used for login with permission.
     *
     * @return Collection<String> which is for getting all persmissions.
     */
    public Collection<String> getPermissions() {
        return permissions;
    }

    /**
     * Set all permissions before login. If not set then it will login with read permission.
     *
     * @param permissions Set all permission before login.
     */
    public void setPermissions(Collection<String> permissions) {
        this.permissions = permissions;
    }


    private final void doPost(Activity activity) {
        ShareDialog.show(activity, this.getShareContent());
    }

    /**
     * For getting ShareContent which is set before facebook wall post.
     *
     * @return ShareContent
     */
    public ShareContent getShareContent() {
        return this.mShareContent;
    }

    /**
     * Set ShareContent which is set before facebook wall post.
     *
     * @param mShareContent For setting content which is text/image/video
     */
    public void setShareContent(ShareContent mShareContent) {
        this.mShareContent = mShareContent;
    }


}
