package com.flunky.facebook;

import org.json.JSONObject;


/**
 * Used for getting user profile detail using GraphRequest
 */
public interface FacebookInterface {
    /**
     *
     * @param jsonObject All profile detail which is provided by facebook default.
     */
    void getMyProfile(JSONObject jsonObject);
}
