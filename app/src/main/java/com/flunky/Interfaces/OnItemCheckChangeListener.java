package com.flunky.Interfaces;


import android.view.View;

public interface OnItemCheckChangeListener {
    void onItemCheck(int position,boolean is_checked, View view);

}
