package com.flunky.Interfaces;

import android.view.View;

public interface OnRecyclerViewItemClickListener {
    void onItemClick(int position, View view);

}
