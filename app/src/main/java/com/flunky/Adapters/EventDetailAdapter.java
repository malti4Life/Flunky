package com.flunky.Adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.flunky.Interfaces.OnRecyclerViewItemClickListener;
import com.flunky.Model.EventDataModel;
import com.flunky.R;

import java.util.ArrayList;

public class EventDetailAdapter extends RecyclerView.Adapter<EventDetailAdapter.ViewHolder>{
    private Context context;
    private LayoutInflater layoutInflater;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private ArrayList<EventDataModel> eventDataModelArrayList;

    public EventDetailAdapter(Context context, ArrayList<EventDataModel> eventDataModelArrayList) {
        this.context=context;
        this.eventDataModelArrayList=eventDataModelArrayList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public EventDetailAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       final View view = layoutInflater.inflate(R.layout.row_events,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventDetailAdapter.ViewHolder holder, int position) {
        final EventDataModel eventDataModel = eventDataModelArrayList.get(position);
        holder.eventDetailAdapter_tv_event_date.setText(eventDataModel.getEvent_date());
        holder.eventDetailAdapter_tv_event_name.setText(eventDataModel.getEvent_name());

    }

    @Override
    public int getItemCount() {
        return eventDataModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView eventDetailAdapter_tv_event_name;
        private TextView eventDetailAdapter_tv_event_date;
        private TextView eventDetailAdapter_tv_gift_me;
        private ImageView eventDetailAdapter_iv_delete_event;

        public ViewHolder(View itemView) {
            super(itemView);
            eventDetailAdapter_tv_event_date = (TextView)itemView.findViewById(R.id.row_event_tv_date);
            eventDetailAdapter_tv_event_name=(TextView)itemView.findViewById(R.id.row_event_tv_event_name);
            eventDetailAdapter_tv_gift_me=(TextView)itemView.findViewById(R.id.row_event_tv_giftMe);
            eventDetailAdapter_iv_delete_event=(ImageView)itemView.findViewById(R.id.row_event_iv_delete_event);
            eventDetailAdapter_iv_delete_event.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onRecyclerViewItemClickListener!=null){
                        onRecyclerViewItemClickListener.onItemClick(getAdapterPosition(), v);
                    }
                }
            });
            eventDetailAdapter_tv_gift_me.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onRecyclerViewItemClickListener!=null){
                        onRecyclerViewItemClickListener.onItemClick(getAdapterPosition(), v);
                    }
                }
            });
        }
    }
    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener){
        this.onRecyclerViewItemClickListener=onRecyclerViewItemClickListener;
    }
}
