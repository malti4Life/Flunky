package com.flunky.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flunky.Model.RestaurantMenuResponseModel;
import com.flunky.R;
import com.flunky.utils.Utils;

import java.util.ArrayList;
import java.util.zip.Inflater;


public class SeeMenuAdapter extends RecyclerView.Adapter<SeeMenuAdapter.ViewHolder> {
    private LayoutInflater layoutInflater;
    private Context context;
    private ArrayList<RestaurantMenuResponseModel> restaurantMenuResponseModelArrayList;
    public SeeMenuAdapter(Context context,ArrayList<RestaurantMenuResponseModel>restaurantMenuResponseModelArrayList){
        this.context=context;
        layoutInflater = LayoutInflater.from(context);
        this.restaurantMenuResponseModelArrayList=restaurantMenuResponseModelArrayList;

    }
    @Override
    public SeeMenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = layoutInflater.inflate(R.layout.row_see_menu_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SeeMenuAdapter.ViewHolder holder, int position) {
       final RestaurantMenuResponseModel menuResponseModel = restaurantMenuResponseModelArrayList.get(position);
        holder.row_see_menu_tv_heading.setText(Utils.getUpperCaseString(menuResponseModel.getTitle()));
        if(!menuResponseModel.getRestaurantRecommendationResponseModels().isEmpty()){
            Log.e("item", String.valueOf(menuResponseModel.getRestaurantRecommendationResponseModels().size()));
            for(int i=0; i<menuResponseModel.getRestaurantRecommendationResponseModels().size(); i++){
                final LayoutInflater layoutInflater = LayoutInflater.from(context);
                final View subItemView = layoutInflater.inflate(R.layout.row_see_menu_sub_items, null);
                final TextView row_see_menu_tv_item_name = (TextView)subItemView.findViewById(R.id.row_see_menu_tv_item_name);
                final TextView row_see_menu_tv_item_price=(TextView)subItemView.findViewById(R.id.row_see_menu_tv_item_price);

                row_see_menu_tv_item_name.setText(menuResponseModel.getRestaurantRecommendationResponseModels().get(i).getTitle());
                row_see_menu_tv_item_price.setText(menuResponseModel.getRestaurantRecommendationResponseModels().get(i).getPrice());
                holder.row_see_menu_ll_items.addView(subItemView);
            }

        }
    }

    @Override
    public int getItemCount() {
        return restaurantMenuResponseModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView row_see_menu_tv_heading;
        private LinearLayout row_see_menu_ll_items;
        public ViewHolder(View itemView) {
            super(itemView);
            row_see_menu_tv_heading=(TextView)itemView.findViewById(R.id.row_see_menu_tv_title);
            row_see_menu_ll_items=(LinearLayout)itemView.findViewById(R.id.row_see_menu_ll_items);
        }
    }
}
