package com.flunky.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flunky.Interfaces.OnRecyclerViewItemClickListener;
import com.flunky.Model.RestaurantRecommendationResponseModel;
import com.flunky.Model.RestaurantResponseModel;
import com.flunky.R;
import com.flunky.webservices.ApiConstant;
import com.flunky.widgets.AppImageView;


import java.util.ArrayList;

public class SearchRecyclerViewAdapter extends RecyclerView.Adapter<SearchRecyclerViewAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private ArrayList<RestaurantResponseModel> restaurantResponseModelArrayList;

    public SearchRecyclerViewAdapter(Context context, ArrayList<RestaurantResponseModel> restaurantResponseModelArrayList) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.restaurantResponseModelArrayList = restaurantResponseModelArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = inflater.inflate(R.layout.row_fragment_search, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchRecyclerViewAdapter.ViewHolder holder, int position) {

        String imageUrl = restaurantResponseModelArrayList.get(position).getRest_img();
        imageUrl = ApiConstant.BASE_URL + imageUrl;
        final RestaurantResponseModel restaurantResponseModel = restaurantResponseModelArrayList.get(position);
        if (restaurantResponseModel.isHas_more_clicked()) {

            holder.row_fragment_seaarch_ll_more.setVisibility(View.VISIBLE);
            holder.row_fragment_search_iv_more_close.setVisibility(View.VISIBLE);
            holder.row_fragment_search_iv_more.setVisibility(View.GONE);


        } else {
            holder.row_fragment_seaarch_ll_more.setVisibility(View.GONE);
            holder.row_fragment_search_iv_more_close.setVisibility(View.GONE);
            holder.row_fragment_search_iv_more.setVisibility(View.VISIBLE);
        }
        holder.row_fragment_search_tv_restro_name.setText(restaurantResponseModel.getRest_title());
        holder.row_fragment_search_tv_restro_address.setText(restaurantResponseModel.getRest_address());
        holder.row_fragment_search_iv_restro_image.setCircleImage(false);
        holder.row_fragment_search_iv_restro_image.loadImage(imageUrl);
        holder.row_fragment_search_tv_cusine.setText(restaurantResponseModel.getRest_cuisine().toUpperCase());
        holder.row_fragment_search_tv_cusine_price.setText(restaurantResponseModel.getRest_cost());
        for (int i = 0; i < restaurantResponseModelArrayList.get(position).getRestaurantRecommendationResponseModel().size(); i++) {
            if (i == 0) {
                final RestaurantRecommendationResponseModel restaurantRecommendationResponseModelTwo = restaurantResponseModelArrayList.get(position).getRestaurantRecommendationResponseModel().get(i);
                holder.row_fragment_search_tv_dinner_tow.setText(restaurantRecommendationResponseModelTwo.getTitle().toUpperCase());
                holder.row_fragment_search_tv_dinner_two_description.setText(restaurantRecommendationResponseModelTwo.getDesc());
                holder.row_fragment_search_tv_dinner_two_price.setText(context.getResources().getString(R.string.price_point) + restaurantRecommendationResponseModelTwo.getPrice());
            }
            if (i == 1) {
                final RestaurantRecommendationResponseModel restaurantRecommendationResponseModelFour = restaurantResponseModelArrayList.get(position).getRestaurantRecommendationResponseModel().get(i);
                holder.row_fragment_search_tv_dinner_four.setText(restaurantRecommendationResponseModelFour.getTitle().toUpperCase());
                holder.row_fragment_search_tv_dinner_four_description.setText(restaurantRecommendationResponseModelFour.getDesc());
                holder.row_fragment_search_tv_dinner_four_price.setText(context.getResources().getString(R.string.price_point) + restaurantRecommendationResponseModelFour.getPrice());
            }

        }


        if (restaurantResponseModel.is_exclusive().equalsIgnoreCase("1")) {
            holder.row_fragment_search_tv_exclusive.setVisibility(View.VISIBLE);
        } else {
            holder.row_fragment_search_tv_exclusive.setVisibility(View.GONE);
        }
        if (restaurantResponseModel.is_wishlist().equalsIgnoreCase("1")) {
            holder.row_fragment_search_iv_wishlist_red.setVisibility(View.VISIBLE);
            holder.row_fragment_search_iv_wishlist_white.setVisibility(View.GONE);
        } else {
            holder.row_fragment_search_iv_wishlist_red.setVisibility(View.GONE);
            holder.row_fragment_search_iv_wishlist_white.setVisibility(View.VISIBLE);
        }
        holder.row_fragment_search_iv_more.setTag(restaurantResponseModel);
        holder.row_fragment_search_iv_more_close.setTag(restaurantResponseModel);
        holder.row_fragment_search_iv_wishlist_red.setTag(restaurantResponseModel);
        holder.row_fragment_search_iv_wishlist_white.setTag(restaurantResponseModel);

    }

    @Override
    public int getItemCount() {
        return restaurantResponseModelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private LinearLayout row_fragment_search_ll_parent;
        private AppImageView row_fragment_search_iv_restro_image;
        private ImageView row_fragment_search_iv_more;
        private ImageView row_fragment_search_iv_more_close;

        private TextView row_fragment_search_tv_exclusive;
        private TextView row_fragment_search_tv_restro_name;
        private TextView row_fragment_search_tv_restro_address;
        private TextView row_fragment_search_tv_cusine;
        private TextView row_fragment_search_tv_cusine_price;

        private LinearLayout row_fragment_search_ll_gift_me;
        private LinearLayout row_fragment_seaarch_ll_more;

        private TextView row_fragment_search_tv_dinner_tow;
        private TextView row_fragment_search_tv_dinner_two_description;
        private TextView row_fragment_search_tv_dinner_two_price;
        private TextView row_fragment_search_tv_dinner_four;
        private TextView row_fragment_search_tv_dinner_four_description;
        private TextView row_fragment_search_tv_dinner_four_price;

        private ImageView row_fragment_search_iv_see_menu;
        private ImageView row_fragment_search_iv_wishlist_white;
        private ImageView row_fragment_search_iv_wishlist_red;

        public ViewHolder(View itemView) {
            super(itemView);
            row_fragment_search_ll_parent = (LinearLayout) itemView.findViewById(R.id.row_fragment_search_ll_parent);
            row_fragment_seaarch_ll_more = (LinearLayout) itemView.findViewById(R.id.row_fragment_search_more);
            row_fragment_search_ll_gift_me = (LinearLayout) itemView.findViewById(R.id.row_fragment_search_ll_gift_me);
            
            row_fragment_search_iv_restro_image = (AppImageView) itemView.findViewById(R.id.row_fragment_search_iv_restro_image);
            row_fragment_search_iv_see_menu = (ImageView) itemView.findViewById(R.id.row_fragment_search_iv_see_menu);
            row_fragment_search_iv_wishlist_white = (ImageView) itemView.findViewById(R.id.row_fragment_search_iv_wishlist_white);
            row_fragment_search_iv_wishlist_red = (ImageView) itemView.findViewById(R.id.row_fragment_search_iv_wishlist_red);
            row_fragment_search_iv_more = (ImageView) itemView.findViewById(R.id.row_fragment_seaarch_iv_more);
            row_fragment_search_iv_more_close = (ImageView) itemView.findViewById(R.id.row_fragment_seaarch_iv_more_close);

            row_fragment_search_tv_exclusive = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_exclusive);
            row_fragment_search_tv_restro_name = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_restro_name);
            row_fragment_search_tv_restro_address = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_restro_address);
            row_fragment_search_tv_cusine = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_cusine);
            row_fragment_search_tv_cusine_price = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_cusine_cost);



            row_fragment_search_tv_dinner_tow = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_dinner_two);
            row_fragment_search_tv_dinner_two_description = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_dinner_two_description);
            row_fragment_search_tv_dinner_two_price = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_dinner_two_price);
            row_fragment_search_tv_dinner_four = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_dinner_four);
            row_fragment_search_tv_dinner_four_description = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_dinner_four_description);
            row_fragment_search_tv_dinner_four_price = (TextView) itemView.findViewById(R.id.row_fragment_search_tv_dinner_four_price);


            row_fragment_search_iv_see_menu.setOnClickListener(this);
            row_fragment_search_ll_gift_me.setOnClickListener(this);
            row_fragment_search_iv_wishlist_red.setOnClickListener(this);
            row_fragment_search_iv_wishlist_white.setOnClickListener(this);
            row_fragment_search_ll_parent.setOnClickListener(this);
            row_fragment_search_iv_more.setOnClickListener(this);
            row_fragment_search_iv_more_close.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onRecyclerViewItemClickListener != null)
                onRecyclerViewItemClickListener.onItemClick(getAdapterPosition(), view);
        }
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener) {
        this.onRecyclerViewItemClickListener = onRecyclerViewItemClickListener;
    }
}
