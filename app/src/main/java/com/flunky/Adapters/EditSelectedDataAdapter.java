package com.flunky.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.flunky.FlunkyApplication;
import com.flunky.Interfaces.OnRecyclerViewItemClickListener;
import com.flunky.Model.RestaurantSortMenuItemsModel;
import com.flunky.R;

import java.util.ArrayList;


public class EditSelectedDataAdapter extends RecyclerView.Adapter<EditSelectedDataAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater layoutInflater;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private ArrayList<RestaurantSortMenuItemsModel> ocassionCostList;
    public EditSelectedDataAdapter(Context context, ArrayList<RestaurantSortMenuItemsModel>ocassionCOstList){
        this.context=context;
        this.ocassionCostList=ocassionCOstList;
        layoutInflater = LayoutInflater.from(context);

    }
    @Override
    public EditSelectedDataAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       final View view = layoutInflater.inflate(R.layout.row_edit_data_items,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EditSelectedDataAdapter.ViewHolder holder, int position) {
        final RestaurantSortMenuItemsModel data = ocassionCostList.get(position);
        switch (FlunkyApplication.getInstance().getTabType()){
            case COUPANPRICE:
                holder.edit_selected_data_tv.setText(context.getResources().getString(R.string.dollar)+" "+data.getValue());
                break;
            case OCCASSION:
                holder.edit_selected_data_tv.setText(data.getValue());
                break;
        }

    }

    @Override
    public int getItemCount() {
        return ocassionCostList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView edit_selected_data_tv;
        public ViewHolder(View itemView) {
            super(itemView);
            edit_selected_data_tv = (TextView)itemView.findViewById(R.id.row_edit_data_tv);
            edit_selected_data_tv.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(onRecyclerViewItemClickListener!=null){
                onRecyclerViewItemClickListener.onItemClick(getAdapterPosition(),v);
            }
        }
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener){
        this.onRecyclerViewItemClickListener=onRecyclerViewItemClickListener;
    }
}
