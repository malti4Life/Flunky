package com.flunky.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flunky.Interfaces.OnRecyclerViewItemClickListener;
import com.flunky.Model.WalletDetailsModel;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Utils;
import com.flunky.webservices.ApiConstant;
import com.flunky.widgets.AppImageView;

import java.util.ArrayList;

public class WalletRecyclerViewAdapter extends RecyclerView.Adapter<WalletRecyclerViewAdapter.ViewHolder>  {
    private Context context;
    private LayoutInflater inflater;
    private OnRecyclerViewItemClickListener onRecyclerViewItemClickListener;
    private ArrayList<WalletDetailsModel>walletDetailsModelArrayList;
    public WalletRecyclerViewAdapter(Context context,ArrayList<WalletDetailsModel>walletDetailsModelArrayList){
        this.context=context;
        this.walletDetailsModelArrayList=walletDetailsModelArrayList;
        inflater=LayoutInflater.from(context);
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
       final View v = inflater.inflate(R.layout.row_fragment_wallet,parent,false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final WalletDetailsModel walletDetailsModel = walletDetailsModelArrayList.get(position);
        if(walletDetailsModel.isHas_more_clicked()){
            holder.row_fragment_wallet_ll_more.setVisibility(View.VISIBLE);
            holder.row_fragment_wallet_iv_more_close.setVisibility(View.VISIBLE);
            holder. row_fragment_wallet_iv_more.setVisibility(View.GONE);
        }else{
            holder.row_fragment_wallet_ll_more.setVisibility(View.GONE);
            holder.row_fragment_wallet_iv_more_close.setVisibility(View.GONE);
            holder.row_fragment_wallet_iv_more.setVisibility(View.VISIBLE);
        }if(!TextUtils.isEmpty(walletDetailsModel.getImage_one())){
            final String imageUrl = ApiConstant.BASE_URL+walletDetailsModel.getImage_one();
            holder.row_fragment_wallet_iv_rest_img.loadImage(imageUrl);
        }else{
            final String imageUrl = ApiConstant.BASE_URL+walletDetailsModel.getRest_img();
            holder.row_fragment_wallet_iv_rest_img.loadImage(imageUrl);
        }
        if(!TextUtils.isEmpty(walletDetailsModel.getReedem_qr_code())){
            holder.row_fragment_wallet_iv_qr_code.setCircleImage(false);
            holder.row_fragment_wallet_iv_qr_code.loadImage(ApiConstant.BASE_URL+walletDetailsModel.getReedem_qr_code());
        }else{
            holder.row_fragment_wallet_iv_qr_code.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(walletDetailsModel.getBalance_amount())){
            holder.row_fragment_wallet_tv_balance_amount.setVisibility(View.VISIBLE);
            holder.row_fragment_wallet_tv_balance_amount.setText(context.getResources().getString(R.string.dollar)+Utils.rupeeFormat(walletDetailsModel.getBalance_amount()));
        }else{
            holder.row_fragment_wallet_tv_balance_amount.setVisibility(View.GONE);
        }
        if(!TextUtils.isEmpty(walletDetailsModel.getPersonal_msg())){
            holder.row_fragment_wallet_tv_message.setText(Utils.getUpperCaseString(walletDetailsModel.getPersonal_msg()));
        }
        holder.row_fragment_wallet_tv_rest_name.setText(walletDetailsModel.getRest_title());
        holder.row_fragment_wallet_tv_expiry_date.setText(context.getResources().getString(R.string.expires)+" "+walletDetailsModel.getExpiry_date());
        holder.row_fragment_wallet_tv_rest_address.setText(walletDetailsModel.getRest_address());
        holder.row_fragment_wallet_tv_sender_name.setText(context.getResources().getString(R.string.from_sender)+" "+walletDetailsModel.getSender_name().toUpperCase());
        holder.row_fragment_wallet_tv_coupan_code.setText(walletDetailsModel.getRedeem_cofe().toUpperCase());
        holder.row_fragment_wallet_tv_occassion_name.setText(walletDetailsModel.getOccasion().toUpperCase());

        holder.row_fragment_wallet_iv_more.setTag(walletDetailsModel);
        holder.row_fragment_wallet_iv_more_close.setTag(walletDetailsModel);
    }

    @Override
    public int getItemCount() {
        return walletDetailsModelArrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private FrameLayout row_fragment_fl;
        private LinearLayout row_fragment_wallet_ll_more;
        private ImageView row_fragment_wallet_iv_more;
        private ImageView row_fragment_wallet_iv_more_close;
        private AppImageView row_fragment_wallet_iv_rest_img;
        private AppImageView row_fragment_wallet_iv_qr_code;
        private TextView row_fragment_wallet_tv_balance_amount;
        private TextView row_fragment_wallet_tv_rest_name;
        private TextView row_fragment_wallet_tv_expiry_date;
        private TextView row_fragment_wallet_tv_rest_address;
        private TextView row_fragment_wallet_tv_sender_name;
        private TextView row_fragment_wallet_tv_occassion_name;
        private TextView row_fragment_wallet_tv_message;
        private TextView row_fragment_wallet_tv_coupan_code;
        private TextView row_fragment_wallet_tv_book_restro_now;

        public ViewHolder(View itemView) {
            super(itemView);
            row_fragment_fl=(FrameLayout)itemView.findViewById(R.id.row_fragment_wallet_fl);
            row_fragment_wallet_ll_more=(LinearLayout)itemView.findViewById(R.id.row_fragment_wallet_ll_more);
            row_fragment_wallet_iv_more =(ImageView) itemView.findViewById(R.id.row_fragment_wallet_iv_more);
            row_fragment_wallet_iv_more_close =(ImageView)itemView.findViewById(R.id.row_fragment_wallet_iv_more_close);
            row_fragment_wallet_iv_qr_code=(AppImageView)itemView.findViewById(R.id.row_fragment_wallet_iv_qr_code);
            row_fragment_wallet_iv_rest_img=(AppImageView)itemView.findViewById(R.id.row_fragment_wallet_rest_image);
            row_fragment_wallet_tv_balance_amount=(TextView)itemView.findViewById(R.id.row_fragment_wallet_balance);
            row_fragment_wallet_tv_rest_name=(TextView)itemView.findViewById(R.id.row_fragment_wallet_tv_rest_name);
            row_fragment_wallet_tv_expiry_date=(TextView)itemView.findViewById(R.id.row_fragment_wallet_tv_expiry_date);
            row_fragment_wallet_tv_rest_address=(TextView)itemView.findViewById(R.id.row_fragment_wallet_tv_restro_address);
            row_fragment_wallet_tv_sender_name=(TextView)itemView.findViewById(R.id.row_fragment_wallet_tv_sender_name);
            row_fragment_wallet_tv_occassion_name=(TextView)itemView.findViewById(R.id.row_fragment_wallet_tv_occassion_name);
            row_fragment_wallet_tv_message=(TextView)itemView.findViewById(R.id.row_fragment_wallet_tv_personal_message);
            row_fragment_wallet_tv_coupan_code=(TextView)itemView.findViewById(R.id.row_fragment_wallet_tv_coupan_code);
            row_fragment_wallet_tv_book_restro_now=(TextView)itemView.findViewById(R.id.row_fragment_wallet_tv_book_now);

            row_fragment_wallet_iv_rest_img.setCircleImage(false);
            row_fragment_fl.setOnClickListener(this);
            row_fragment_wallet_iv_more.setOnClickListener(this);
            row_fragment_wallet_iv_more_close.setOnClickListener(this);
            row_fragment_wallet_tv_book_restro_now.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onRecyclerViewItemClickListener != null)
                onRecyclerViewItemClickListener.onItemClick(getAdapterPosition(), view);
        }
    }
    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener onRecyclerViewItemClickListener){
        this.onRecyclerViewItemClickListener=onRecyclerViewItemClickListener;
    }
}
