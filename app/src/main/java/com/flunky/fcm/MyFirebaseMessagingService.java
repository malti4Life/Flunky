package com.flunky.fcm;


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;

import com.flunky.Activities.MainActivity;
import com.flunky.Activities.SplashActivity;
import com.flunky.R;
import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private NotificationManager notificationManager;
    private String message = "No message found to display.";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        super.onMessageReceived(remoteMessage);
        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        final Map<String, String> data = remoteMessage.getData();
        if (data.get("msg") != null) {
            message = data.get("msg");
            Log.e("Msg",message);
            sendNotification(null);
        }

    }

    private void sendNotification(Bitmap bitmap) {
        final Vibrator vibrator = (Vibrator)getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        vibrator.vibrate(500);
        {
            final String title = getString(R.string.app_name);
            final Intent intent = new Intent(this,MainActivity.class);
            intent.putExtra("FromNotification","FromNotification");
            final PendingIntent pendintIntent = PendingIntent.getActivity(this,0,intent,PendingIntent.FLAG_UPDATE_CURRENT);
            final NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(Utils.getNotificationIcon())
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setLights(Color.MAGENTA,500,500)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setVibrate(new long[]{500,500})
                    .setContentIntent(pendintIntent);
            if (bitmap != null) {
                final NotificationCompat.BigPictureStyle notiStyle = new NotificationCompat.BigPictureStyle();
                notiStyle.setBigContentTitle(title);
                notiStyle.setSummaryText(message);
                notiStyle.bigPicture(bitmap);
                notificationBuilder.setStyle(notiStyle);
            } else {
                notificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
            }
            notificationManager.notify(0, notificationBuilder.build());

        }
    }
}
