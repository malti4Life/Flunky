package com.flunky.fcm;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.flunky.utils.Constant;
import com.flunky.utils.Logger;
import com.flunky.utils.Utils;
import com.google.firebase.iid.FirebaseInstanceId;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 */
public class FCMRegistrationIntentService extends IntentService {

    public FCMRegistrationIntentService() {
        super("GCMRegistrationIntentService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        final String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Utils.storeString(this, Constant.GCM_UNIQUE_ID, refreshedToken);
        Logger.e(refreshedToken);
    }
}
